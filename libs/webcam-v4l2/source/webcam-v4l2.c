#include "webcam-v4l2.h"

#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <linux/videodev2.h>
#include <turbojpeg.h>

#define TRUE 1
#define FALSE 0

struct webcam_buffer {
    uint8_t* data;
    size_t   size;
};


struct webcam_device {
    int                       fd;
    const char*               video;

    enum webcam_capture_state capture_state;
    pthread_t                 capture_thread;
    pthread_mutex_t           capture_lock;

    FILE*                     errorstream;
    webcam_video_callback     capture_callback;
    void*                     capture_data;
    struct webcam_format      capture_format;

    struct webcam_buffer*     buffer;
    size_t                    buffer_count;

    tjhandle                  jpeg_decoder;
};



static void
release_capture_buffer(struct webcam_device* device)
{
    int n;

    for(n = 0; n < device->buffer_count; ++n) {
        if(device->buffer[n].data > 0)
            free(device->buffer[n].data);
    }

    free(device->buffer);

    device->buffer_count = 0;
    device->buffer = 0;
}


static void
allocate_capture_buffer(struct webcam_device* device, size_t buffer_count, size_t buffer_size)
{
    int n;

    device->buffer_count = buffer_count;
    device->buffer       = calloc(buffer_count, sizeof(struct webcam_buffer));

    for(n = 0; n < device->buffer_count; ++n) {
        device->buffer[n].data = malloc(buffer_size * sizeof(uint8_t));
        device->buffer[n].size = buffer_size;
    }

}



struct webcam_device*   
webcam_device_open_with_errorstream(const char* video, FILE* err)
{
    int fd = open(video, O_RDWR); 

    struct v4l2_capability cap;

    if(fd < 0) {
        fprintf(err, "Device %s could not be opened\n", video);
        return 0;
    }

    if(ioctl(fd, VIDIOC_QUERYCAP, &cap) < 0) {
        fprintf(err, "Device %s could not load V4L2 capability descriptions\n", video);
        return 0;
    }

    if(!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
        fprintf(err, "Device %s does not support V4L2_CAP_VIDEO_CAPTURE capability which is required by webcam-v4l2\n", video);
        return 0;
    }

    if(!(cap.capabilities & V4L2_CAP_STREAMING)) {
        fprintf(err, "Device %s does not support V4L2_CAP_STREAMING capability which is required by webcam-v4l2\n", video);
        return 0;
    }

    struct webcam_device* device = malloc(sizeof(struct webcam_device));

    device->fd = fd;
    device->video = video;
    device->jpeg_decoder = tjInitDecompress();

    device->capture_thread = 0;
    device->capture_state  = IDLE;
    
    device->capture_callback = 0;
    device->capture_data = 0;
    device->errorstream = err;
    device->buffer_count = 0;
    device->buffer = 0;

    pthread_mutex_init(&device->capture_lock, 0);

    return device;
}



struct webcam_device*
webcam_device_open(const char* video)
{
    FILE* outstream = fopen("/dev/null", "w");

    if (outstream)
        return webcam_device_open_with_errorstream(video, outstream);
    else
        return 0;
}



int
webcam_device_close(struct webcam_device* device)
{
    int result = close(device->fd);

    tjDestroy(device->jpeg_decoder);

    if(device->buffer > 0)
        release_capture_buffer(device);
    
    if(result < 0) 
        fprintf(device->errorstream, "Device %s could not be closed\n", device->video);
    else {
        if (device->errorstream != stdout && device->errorstream != stderr && device->errorstream > 0)
            fclose(device->errorstream);

        free(device);
    }

    return result;
}




static void
set_synchronized_capture_state(struct webcam_device* device, enum webcam_capture_state state)
{
    pthread_mutex_lock(&device->capture_lock);
    device->capture_state = state;
    pthread_mutex_unlock(&device->capture_lock);
}


static enum webcam_capture_state
get_synchronized_capture_state(struct webcam_device* device)
{
    enum webcam_capture_state state;

    pthread_mutex_lock(&device->capture_lock);
    state = device->capture_state;
    pthread_mutex_unlock(&device->capture_lock);

    return state;
}


enum webcam_capture_state   
webcam_device_get_capture_state(struct webcam_device* device)
{
    return get_synchronized_capture_state(device);
}


static void* 
captureloop(struct webcam_device* device)
{
    enum v4l2_buf_type buftype = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    struct v4l2_buffer buffer;

    // activate V4L2 streaming I/O
    if(ioctl(device->fd, VIDIOC_STREAMON, &buftype) < 0) {
        set_synchronized_capture_state(device, ERROR_V4L2_STREAMING);
        pthread_exit(0);
    } else 
        set_synchronized_capture_state(device, RUNNING);

    // run capture loop
    struct timeval ts;

    while(get_synchronized_capture_state(device) == RUNNING) {
        memset(&buffer, 0, sizeof(buffer));
        buffer.type   = buftype;
        buffer.memory = V4L2_MEMORY_USERPTR;

        if(ioctl(device->fd, VIDIOC_DQBUF, &buffer) < 0) {
            set_synchronized_capture_state(device, ERROR_V4L2_QUEUE);
            break;
        }

        gettimeofday(&ts, 0);
        uint64_t timestamp = ts.tv_sec * 1000000ULL + ts.tv_usec;

        device->capture_callback(buffer.sequence, (uint8_t*) buffer.m.userptr, buffer.bytesused, timestamp, &device->capture_format, device->capture_data);

        if(ioctl(device->fd, VIDIOC_QBUF, &buffer) < 0) {
            set_synchronized_capture_state(device, ERROR_V4L2_QUEUE);
            break;
        }          
    }

    // deactive V4L2 streaming I/O and release buffers
    if(ioctl(device->fd, VIDIOC_STREAMOFF, &buftype) < 0) {
        set_synchronized_capture_state(device, ERROR_V4L2_STREAMING);
    } else
        set_synchronized_capture_state(device, IDLE);
  
    return NULL;  
}



int
webcam_device_start_capture(struct webcam_device* device, size_t buffer_count, webcam_video_callback callback, void* data)
{
    if(get_synchronized_capture_state(device) == RUNNING) {
        fprintf(device->errorstream, "Webcam device %s is already running\n", device->video);
        return FALSE;
    }

    struct v4l2_requestbuffers request;
    struct v4l2_buffer buffer;
    struct v4l2_format format;

    memset(&request, 0, sizeof(request));
    memset(&format, 0, sizeof(format));

    //retrive current format information for getting pixelformat size
    format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if(ioctl(device->fd, VIDIOC_G_FMT, &format) < 0) {
        fprintf(device->errorstream, "Webcam device %s could not retrieve format information from V4L2\n", device->video);
        set_synchronized_capture_state(device, ERROR_V4L2_BUFFER);
        return FALSE;
    }

    // configure V4L2 buffers with kernel-spaced memory 
    request.count = buffer_count;
    request.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    request.memory = V4L2_MEMORY_USERPTR;

    if(ioctl(device->fd, VIDIOC_REQBUFS, &request) < 0) {
        fprintf(device->errorstream, "Webcam device %s could not request a user-spaced V4L2 buffer\n", device->video);
        set_synchronized_capture_state(device, ERROR_V4L2_BUFFER);
        return FALSE;
    }

    // allocate and setup V4L2 buffers
    if(device->buffer > 0)
        release_capture_buffer(device);
    
    allocate_capture_buffer(device, request.count, format.fmt.pix.sizeimage);

    int n;

    for(n = 0; n < request.count; n++) {
        memset(&buffer, 0, sizeof(buffer));
        buffer.type      = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buffer.memory    = V4L2_MEMORY_USERPTR; 
        buffer.index     = n;
        buffer.m.userptr = (long) device->buffer[n].data;
        buffer.length    = device->buffer[n].size;

        if(ioctl(device->fd, VIDIOC_QBUF, &buffer) < 0) {
            fprintf(device->errorstream, "Webcam device %s could not add buffer %d to V4L2 kernel\n", device->video, n);
            set_synchronized_capture_state(device, ERROR_V4L2_BUFFER);
            return FALSE;
        }
    }       

    // run thread with capture loop
    set_synchronized_capture_state(device, IDLE);

    device->capture_callback = callback;
    device->capture_data = data;

    if (!webcam_device_get_pixelformat_details(device, &device->capture_format)) {
        fprintf(device->errorstream, "Webcam device %s could not retrieve capture pixelformat\n", device->video);
        return FALSE;
    }

    if(pthread_create(&device->capture_thread, 0, (void* (*)(void*)) &captureloop, device) != 0) {
        fprintf(device->errorstream, "Webcam device %s could not create capture thread\n", device->video);
        return FALSE;
    }

    return TRUE;
}

static uint32_t
v4l2_cid(enum webcam_control control) {
    switch(control) {
        case CONTROL_BRIGHTNESS:
            return V4L2_CID_BRIGHTNESS;

        case CONTROL_CONTRAST:
            return V4L2_CID_CONTRAST;

        case CONTROL_SATURATION:
            return V4L2_CID_SATURATION;

        case CONTROL_WHITE_BALANCE:
            return V4L2_CID_WHITE_BALANCE_TEMPERATURE;

        case CONTROL_GAIN:
            return V4L2_CID_GAIN;

        case CONTROL_POWER_LINE_FREQUENCY:
            return V4L2_CID_POWER_LINE_FREQUENCY;

        case CONTROL_SHARPNESS:
            return V4L2_CID_SHARPNESS;

        case CONTROL_BACKLIGHT_COMPENSATION:
            return V4L2_CID_BACKLIGHT_COMPENSATION;

        case CONTROL_ZOOM:
            return V4L2_CID_ZOOM_ABSOLUTE;

        case CONTROL_PAN:
            return V4L2_CID_PAN_ABSOLUTE;

        case CONTROL_TILT:
            return V4L2_CID_TILT_ABSOLUTE;

        case CONTROL_FOCUS:
            return V4L2_CID_FOCUS_ABSOLUTE;

        case CONTROL_EXPOSURE:
            return V4L2_CID_EXPOSURE_ABSOLUTE;

        case CONTROL_AUTO_EXPOSURE:
            return V4L2_CID_EXPOSURE_AUTO;

        case CONTROL_AUTO_WHITE_BALANCE:
            return V4L2_CID_AUTO_WHITE_BALANCE;

        case CONTROL_AUTO_FOCUS:
            return V4L2_CID_FOCUS_AUTO;
    }

    return 0;
}

int 
webcam_device_stop_capture(struct webcam_device* device)
{
    if(get_synchronized_capture_state(device) == RUNNING) {
        set_synchronized_capture_state(device, IDLE);
    }

    return (pthread_join(device->capture_thread, 0) == 0);
}


int
webcam_control_query(struct webcam_device* device, enum webcam_control control, int* min, int* max, int* step, int* def)
{
    struct v4l2_queryctrl ctrl;
    memset(&ctrl, 0, sizeof(ctrl));

    ctrl.id = v4l2_cid(control);

    if(ioctl(device->fd, VIDIOC_QUERYCTRL, &ctrl) < 0)
        return FALSE;

    if(min > 0)
        *min  = ctrl.minimum;

    if(max > 0)
        *max  = ctrl.maximum;

    if(step > 0) 
        *step = ctrl.step;

    if(def > 0)
        *def  = ctrl.default_value;

    return TRUE;
}



int 
webcam_device_set_pixelformat(struct webcam_device* device, enum webcam_pixelformat pixformat, size_t width, size_t height)
{
    struct v4l2_format format;
    memset(&format, 0, sizeof(format));

    format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    format.fmt.pix.width  = width;
    format.fmt.pix.height = height;

    switch(pixformat) {
        case YUYV:
            format.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
            break;

        case MJPEG:
            format.fmt.pix.pixelformat = V4L2_PIX_FMT_MJPEG;
            break;

        default:
            fprintf(device->errorstream, "Unknown pixelformat is given to set_pixelformat\n");
            return FALSE;
            break;
    }

    return (ioctl(device->fd, VIDIOC_S_FMT, &format) == 0);
}

    
int 
webcam_device_get_pixelformat(struct webcam_device* device, enum webcam_pixelformat* pixformat, size_t* width, size_t* height)
{
    struct v4l2_format format;
    memset(&format, 0, sizeof(format));

    format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if(ioctl(device->fd, VIDIOC_G_FMT, &format) < 0) {
        fprintf(device->errorstream, "Could not read current pixelformat from V4L2\n");
        return FALSE;
    }

    if (pixformat > 0) {
        switch(format.fmt.pix.pixelformat) {
            case V4L2_PIX_FMT_YUYV:
                *pixformat = YUYV;
                break;

            case V4L2_PIX_FMT_MJPEG:
                *pixformat = MJPEG;
                break;

            default:
                *pixformat = UNKNOWN;
                fprintf(device->errorstream, "Unknown pixformat found in reading current pixelformat\n");
                break;
        }
    }

    if (width > 0) {
        *width  = format.fmt.pix.width;
        *height = format.fmt.pix.height;
    }

    return TRUE;
}

int 
webcam_device_get_pixelformat_details(struct webcam_device* device, struct webcam_format* out)
{
    struct v4l2_format format;
    memset(&format, 0, sizeof(format));

    format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if(ioctl(device->fd, VIDIOC_G_FMT, &format) < 0) {
        fprintf(device->errorstream, "Could not read current pixelformat from V4L2\n");
        return FALSE;
    }

    if (out > 0) {
        switch(format.fmt.pix.pixelformat) {
            case V4L2_PIX_FMT_YUYV:
                out->pixelformat = YUYV;
                break;

            case V4L2_PIX_FMT_MJPEG:
                out->pixelformat = MJPEG;
                break;

            default:
                out->pixelformat = UNKNOWN;
                fprintf(device->errorstream, "Unknown pixformat found in reading current pixelformat\n");
                break;
        }

        out->width  = format.fmt.pix.width;
        out->height = format.fmt.pix.height;
        out->bytes_per_line = format.fmt.pix.bytesperline;
        out->max_size = format.fmt.pix.sizeimage;
    }

    return TRUE;
}





int
webcam_device_set_framerate(struct webcam_device* device, int fps)
{
    struct v4l2_streamparm p;
    memset(&p, 0, sizeof(p));

    p.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if(ioctl(device->fd, VIDIOC_G_PARM, &p) < 0) {
        fprintf(device->errorstream, "Could not read current fps value from V4L2\n");
        return FALSE;
    }

    p.parm.capture.timeperframe.denominator = fps;
    p.parm.capture.timeperframe.numerator = 1;

    if(ioctl(device->fd, VIDIOC_S_PARM, &p) < 0) {
        fprintf(device->errorstream, "Could not write fps value to V4L2\n");
        return FALSE;
    }

    return TRUE;   
}


int
webcam_device_get_framerate(struct webcam_device* device, int* fps)
{
    struct v4l2_streamparm p;
    memset(&p, 0, sizeof(p));

    p.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if(ioctl(device->fd, VIDIOC_G_PARM, &p) < 0) {
        fprintf(device->errorstream, "Could not read current fps value from V4L2\n");
        return FALSE;
    }

    *fps = p.parm.capture.timeperframe.denominator /  p.parm.capture.timeperframe.numerator;

    return TRUE;   
}




int
webcam_control_get(struct webcam_device* device, enum webcam_control control, int* value)
{
    struct v4l2_control ctrl;
    memset(&ctrl, 0, sizeof(ctrl));

    ctrl.id = v4l2_cid(control);
    
    if(ioctl(device->fd, VIDIOC_G_CTRL, &ctrl) < 0) {
        fprintf(device->errorstream, "Could not read value control option %d\n", control);
        return FALSE;
    }

    if (value > 0) 
        *value = ctrl.value;

    return TRUE;
}


int
webcam_control_set(struct webcam_device* device, enum webcam_control control, int value)
{
    struct v4l2_control ctrl;
    memset(&ctrl, 0, sizeof(ctrl));

    ctrl.id = v4l2_cid(control);
    ctrl.value = value;

    if(ioctl(device->fd, VIDIOC_S_CTRL, &ctrl) < 0) {
        fprintf(device->errorstream, "Could not write value control option %d with %d\n", control, value);
        return FALSE;
    }

    return TRUE;
}


int
webcam_control_set_callback(struct webcam_device* device, webcam_control_callback callback, void* hints)
{
    struct v4l2_event_subscription sub;
    memset(&sub, 0, sizeof(sub));

    sub.type = V4L2_EVENT_CTRL;
    sub.flags = V4L2_EVENT_SUB_FL_SEND_INITIAL;

    if(ioctl(device->fd, VIDIOC_SUBSCRIBE_EVENT, &sub) < 0) {
        fprintf(device->errorstream, "Could subscribe control events\n");
        return FALSE;
    }

    return TRUE;
}
        


mjpeg_decoder_t 
mjpeg_decoder_new(void)
{
    return tjInitDecompress();
}

void
mjpeg_decoder_destroy(mjpeg_decoder_t decoder)
{
    tjDestroy(decoder);
}


int
mjpeg_decoder_mjpeg2rgb(mjpeg_decoder_t decoder, uint8_t* data, size_t data_size, uint8_t* target)
{
    int width, height, subsamp;

    tjDecompressHeader2((tjhandle) decoder, data, data_size, &width, &height, &subsamp);
    tjDecompress2((tjhandle) decoder, data, data_size, target, width, 0, height, TJPF_RGB, TJFLAG_FASTDCT);

    return TRUE;
}
