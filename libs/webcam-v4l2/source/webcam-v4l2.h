#include <stdint.h>
#include <stdio.h>
#include <limits.h>

#ifdef __cplusplus
extern "C" {
#endif

struct webcam_device;

typedef void* mjpeg_decoder_t;

#define WEBCAM_FAILURE INT_MIN

enum webcam_pixelformat {
    UNKNOWN = 0,
    YUYV,
    MJPEG
};

enum webcam_capture_state {
    RUNNING,
    IDLE,
    ERROR_V4L2_STREAMING,
    ERROR_V4L2_QUEUE,
    ERROR_V4L2_BUFFER,
    ERROR_V4L2
};


enum webcam_control {
    CONTROL_BRIGHTNESS = 0,
    CONTROL_CONTRAST,
    CONTROL_SATURATION,
    CONTROL_WHITE_BALANCE,
    CONTROL_GAIN,
    CONTROL_POWER_LINE_FREQUENCY,
    CONTROL_SHARPNESS,
    CONTROL_BACKLIGHT_COMPENSATION,
    CONTROL_ZOOM,
    CONTROL_PAN,
    CONTROL_TILT,
    CONTROL_FOCUS,
    CONTROL_EXPOSURE,
    CONTROL_AUTO_EXPOSURE,
    CONTROL_AUTO_WHITE_BALANCE,
    CONTROL_AUTO_FOCUS,
    CONTROL_FPS
};

static const size_t MAX_CONTROL_NUMBERS = CONTROL_FPS + 1;


struct webcam_format {
  uint32_t width;
  uint32_t height;
  uint32_t bytes_per_line;
  uint32_t max_size;
  
  enum webcam_pixelformat pixelformat;
};


typedef void (*webcam_video_callback)(uint32_t image_index, uint8_t* data, size_t size, uint64_t timestamp, struct webcam_format* fmt, void* hints);
typedef void (*webcam_control_callback(enum webcam_control control, int value, void* hints));


struct webcam_device*       webcam_device_open(const char* video);
struct webcam_device*       webcam_device_open_with_errorstream(const char* video, FILE* err);

int                         webcam_device_close(struct webcam_device* device);

int                         webcam_device_start_capture(struct webcam_device* device,
                                                        size_t buffer_numbers,
                                                        webcam_video_callback callback,
                                                        void* hints);

int                         webcam_device_stop_capture(struct webcam_device* device);

enum webcam_capture_state   webcam_device_get_capture_state(struct webcam_device* device);

int                         webcam_device_set_pixelformat(struct webcam_device* device, 
                                                          enum webcam_pixelformat format, 
                                                          size_t width, 
                                                          size_t height);

int                         webcam_device_get_pixelformat(struct webcam_device* device,
                                                          enum webcam_pixelformat* format,
                                                          size_t* width,
                                                          size_t* height);

int                         webcam_device_get_pixelformat_details(struct webcam_device* device,
                                                                  struct webcam_format* format);

int                         webcam_device_set_framerate(struct webcam_device* device, int fps);
int                         webcam_device_get_framerate(struct webcam_device* device, int *fps);


int                         webcam_control_query(struct webcam_device* device, enum webcam_control control, int* min, int* max, int* step, int* def);
int                         webcam_control_get(struct webcam_device* device, enum webcam_control control, int* value);
int                         webcam_control_set(struct webcam_device* device, enum webcam_control control, int value);
int                         webcam_control_set_callback(struct webcam_device* device, webcam_control_callback callback, void* hints);


mjpeg_decoder_t             mjpeg_decoder_new(void);

int                         mjpeg_decoder_mjpeg2rgb(mjpeg_decoder_t decoder, 
                                                    uint8_t* mjpeg_data, 
                                                    size_t mjpeg_size, 
                                                    uint8_t* target); 

void                        mjpeg_decoder_destroy(mjpeg_decoder_t decoder);

#ifdef __cplusplus
}
#endif


