#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include "webcam-v4l2.h"

static void loop(uint32_t image_index, uint8_t* data, size_t size, uint64_t timestamp, struct webcam_format* fmt, void* hints)
{
    printf("%4u %lu %zu\n", image_index, timestamp, size);
    fflush(stdout);
}

int main(int argc, char** argv) {
    struct webcam_device* device = webcam_device_open("/dev/video0");
    
    if(webcam_device_start_capture(device, 8, loop, 0))
        printf("Start done\n");

    sleep(10);

    if(webcam_device_stop_capture(device))
        printf("Stop done\n");

    printf("%d %d", EXIT_FAILURE, EXIT_SUCCESS);
   
    webcam_device_close(device);    

    return EXIT_SUCCESS;
}
