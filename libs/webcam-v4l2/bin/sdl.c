#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>
#include <unistd.h>
#include "webcam-v4l2.h"

static const char* video = "/dev/video0";

static uint8_t* data = 0;
static size_t   size = 0;
static int width         = 1920;
static int height        = 1080;
static int buffers       = 4;

mjpeg_decoder_t decoder;

static void
capture(uint32_t image_index, uint8_t* ptr, size_t ptr_size, uint64_t timestamp, struct webcam_format* fmt, void* data0)
{
    mjpeg_decoder_mjpeg2rgb(decoder, ptr, ptr_size, data);
}


void init_opengl(uint32_t* gl_rgb_tex)
{
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, width, height, 0, 0.0f, 1.0f);
    glMatrixMode(GL_MODELVIEW);

    glClearColor(0.0, 0.0, 0.0, 0.0);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glGenTextures(1, gl_rgb_tex);
    glBindTexture(GL_TEXTURE_2D, *gl_rgb_tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}


void render_image(uint32_t gl_rgb_tex)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, gl_rgb_tex);
    glTexImage2D(GL_TEXTURE_2D, 0, 3, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);

    glBegin(GL_TRIANGLE_FAN);
    glColor4f(1, 1, 1, 1);
    glTexCoord2f(0, 0); glVertex3f(0, 0, 0);
    glTexCoord2f(1, 0); glVertex3f(width, 0, 0);
    glTexCoord2f(1, 1); glVertex3f(width, height, 0);
    glTexCoord2f(0, 1); glVertex3f(0, height, 0);
    glEnd();
}


static int 
fgetInt(FILE* s)
{
    char input[128];
    int  length;
    int choice;

    fgets(input, 128, s);
    length = strlen(input);
    input[length-1] = '\0';

    return atoi(input);
}


static int
webcam_control_get0(struct webcam_device* dev, enum webcam_control c)
{
    int val;
    webcam_control_get(dev, c, &val);
    return val;
}


void* 
inputconsole(struct webcam_device* device)
{
    sleep(2);

    while(1) {
        
        printf("%5s %5d Brightness\n", "(1)", webcam_control_get0(device, CONTROL_BRIGHTNESS));
        printf("%5s %5d Saturation\n", "(2)", webcam_control_get0(device, CONTROL_SATURATION));
        printf("%5s %5d Contrast\n", "(3)", webcam_control_get0(device, CONTROL_CONTRAST));
        printf("%5s %5d White Balance\n", "(4)", webcam_control_get0(device, CONTROL_WHITE_BALANCE));
        printf("%5s %5d Gain\n", "(5)", webcam_control_get0(device, CONTROL_GAIN));
        printf("%5s %5d Sharpness\n", "(6)", webcam_control_get0(device, CONTROL_SHARPNESS));
        printf("%5s %5d Zoom\n", "(7)", webcam_control_get0(device, CONTROL_ZOOM));
        printf("%5s %5d Pan\n", "(8)", webcam_control_get0(device, CONTROL_PAN));
        printf("%5s %5d Tilt\n", "(9)", webcam_control_get0(device, CONTROL_TILT));
        printf("%5s %5d Backlight Compensation\n", "(10)", webcam_control_get0(device, CONTROL_BACKLIGHT_COMPENSATION));
        printf("%5s %5d Focus\n", "(11)", webcam_control_get0(device, CONTROL_FOCUS));
        printf("%5s %5d Exposure\n", "(12)", webcam_control_get0(device, CONTROL_EXPOSURE));
        printf("\n");
        printf("%5s %5d AUTO White Balance\n", "(13)", webcam_control_get0(device, CONTROL_AUTO_WHITE_BALANCE));
        printf("%5s %5d AUTO Focus\n", "(14)", webcam_control_get0(device, CONTROL_AUTO_FOCUS));
        printf("%5s %5d AUTO Exposure\n", "(15)", webcam_control_get0(device, CONTROL_AUTO_EXPOSURE));
        printf("%5s %5d AUTO Power Line Frequency\n", "(16)", webcam_control_get0(device, CONTROL_POWER_LINE_FREQUENCY));
        printf("Auswahl: ");
        fflush(stdout);

        int option = fgetInt(stdin);

        printf("New Value: ");
        fflush(stdout);
        int value = fgetInt(stdin);

        enum webcam_control ctrl;

        switch(option) {
            case 1:    ctrl = CONTROL_BRIGHTNESS; break;
            case 2:    ctrl = CONTROL_SATURATION; break;
            case 3:    ctrl = CONTROL_CONTRAST; break;
            case 4:    ctrl = CONTROL_WHITE_BALANCE; break;
            case 5:    ctrl = CONTROL_GAIN; break;
            case 6:    ctrl = CONTROL_SHARPNESS; break;
            case 7:    ctrl = CONTROL_ZOOM; break;
            case 8:    ctrl = CONTROL_PAN; break;
            case 9:    ctrl = CONTROL_TILT; break;
            case 10:   ctrl = CONTROL_BACKLIGHT_COMPENSATION; break;
            case 11:   ctrl = CONTROL_FOCUS; break;
            case 12:   ctrl = CONTROL_EXPOSURE; break;
            case 13:   ctrl = CONTROL_AUTO_WHITE_BALANCE; break;
            case 14:   ctrl = CONTROL_AUTO_FOCUS; break;
            case 15:   ctrl = CONTROL_AUTO_EXPOSURE; break;
            case 16:   ctrl = CONTROL_POWER_LINE_FREQUENCY; break;
        }

        webcam_control_set(device, ctrl, value);
    }
}



int 
main(int argc, char** argv) 
{   
    SDL_Init(SDL_INIT_VIDEO);

    SDL_Window* window = SDL_CreateWindow("V4L2 Camera", 0, 0, width, height, SDL_WINDOW_OPENGL);
    SDL_GLContext glc  = SDL_GL_CreateContext(window);
    
    decoder = mjpeg_decoder_new();
    size = width * height * 3;
    data = malloc(size * sizeof(uint8_t));
    memset(data, 0, size * sizeof(uint8_t));

    struct webcam_device* device = webcam_device_open(video);
    
    webcam_device_set_pixelformat(device, MJPEG, width, height);
    webcam_device_set_framerate(device, 30);
    
    webcam_device_start_capture(device, 4, capture, 0);

    uint32_t gl_rgb_tex;
    init_opengl(&gl_rgb_tex);

    pthread_t tid;
    pthread_create(&tid, 0, (void* (*)(void*)) &inputconsole, device);

    while(1) {
        SDL_Event event;

        while(SDL_PollEvent(&event)) {
            switch(event.type) {
                case SDL_QUIT:
                    webcam_device_stop_capture(device);
                    goto QuitApplication;
                    break;
            }
        }

        render_image(gl_rgb_tex);

        SDL_GL_SwapWindow(window);        
    }

QuitApplication:
    SDL_GL_DeleteContext(glc);
    SDL_DestroyWindow(window);
    SDL_Quit();

    webcam_device_close(device);
    mjpeg_decoder_destroy(decoder);
    free(data);

    printf("\n");
    fflush(stdout);

    return EXIT_SUCCESS;
}



