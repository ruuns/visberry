#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/videodev2.h>

void quit(int dev, const char* msg) {
    if(msg != 0) 
        fprintf(stderr, "%s", msg);

    close(dev);
    exit(EXIT_FAILURE);
}

void print_querycap(int);
void print_enumfmt(int);
void print_enumframesizes(int, int); 
void print_queryctrl(int);
void print_querycrop(int);

int main(int argc, char** argv) 
{
    if(argc < 2) {
        fprintf(stderr, "No device identifier is specified for the first argument\n");
        exit(EXIT_FAILURE);
    }
    
    int dev = open(argv[1], O_RDONLY);

    if(dev >= 0) {
        print_querycap(dev);
        print_queryctrl(dev);
        print_enumfmt(dev);
        print_querycrop(dev);
        
    } else {
        fprintf(stderr, "device %s could not be opened\n", argv[1]);
        exit(EXIT_FAILURE);
    }

    close(dev);

    return EXIT_SUCCESS;
}


#define PRINT_FLAG(X, Y) \
    if(X & Y) \
        printf(" - " #Y "\n");


void print_querycap(int dev) {
    struct v4l2_capability info;
    
    if(ioctl(dev, VIDIOC_QUERYCAP, &info) < 0)
        quit(dev, "querycap failed for this device");

    printf("=====================================================================================\n");
    printf("QUERYCAP\n");
    printf("=====================================================================================\n");
    printf("Driver:  %s\n", info.driver);
    printf("Device:  %s\n", info.card);
    printf("BusInfo: %s\n", info.bus_info);
    printf("Version: %u.%u.%u\n\n", (info.version >> 16) & 0xFF, (info.version >> 8) & 0xFF, info.version & 0xFF);

    printf("Capabilities:\n");

    PRINT_FLAG(info.capabilities, V4L2_CAP_VIDEO_CAPTURE);
    PRINT_FLAG(info.capabilities, V4L2_CAP_VIDEO_CAPTURE_MPLANE);
    PRINT_FLAG(info.capabilities, V4L2_CAP_VIDEO_OUTPUT);
    PRINT_FLAG(info.capabilities, V4L2_CAP_VIDEO_OUTPUT_MPLANE);
    PRINT_FLAG(info.capabilities, V4L2_CAP_VIDEO_M2M);
    PRINT_FLAG(info.capabilities, V4L2_CAP_VIDEO_M2M_MPLANE);
    PRINT_FLAG(info.capabilities, V4L2_CAP_VIDEO_OVERLAY);
    PRINT_FLAG(info.capabilities, V4L2_CAP_VIDEO_OUTPUT_OVERLAY);
    PRINT_FLAG(info.capabilities, V4L2_CAP_VBI_CAPTURE);
    PRINT_FLAG(info.capabilities, V4L2_CAP_VBI_OUTPUT);
    PRINT_FLAG(info.capabilities, V4L2_CAP_SLICED_VBI_CAPTURE);
    PRINT_FLAG(info.capabilities, V4L2_CAP_SLICED_VBI_OUTPUT);
    PRINT_FLAG(info.capabilities, V4L2_CAP_RDS_CAPTURE);
    PRINT_FLAG(info.capabilities, V4L2_CAP_RDS_OUTPUT);
    PRINT_FLAG(info.capabilities, V4L2_CAP_HW_FREQ_SEEK);
    
    PRINT_FLAG(info.capabilities, V4L2_CAP_TUNER);
    PRINT_FLAG(info.capabilities, V4L2_CAP_AUDIO);
    PRINT_FLAG(info.capabilities, V4L2_CAP_RADIO);
    PRINT_FLAG(info.capabilities, V4L2_CAP_MODULATOR);
    PRINT_FLAG(info.capabilities, V4L2_CAP_SDR_CAPTURE);

    PRINT_FLAG(info.capabilities, V4L2_CAP_READWRITE);
    PRINT_FLAG(info.capabilities, V4L2_CAP_ASYNCIO);
    PRINT_FLAG(info.capabilities, V4L2_CAP_STREAMING);
    PRINT_FLAG(info.capabilities, V4L2_CAP_DEVICE_CAPS);

    printf("\nDevice Capabilities:\n");

    PRINT_FLAG(info.device_caps, V4L2_CAP_VIDEO_CAPTURE);
    PRINT_FLAG(info.device_caps, V4L2_CAP_VIDEO_CAPTURE_MPLANE);
    PRINT_FLAG(info.device_caps, V4L2_CAP_VIDEO_OUTPUT);
    PRINT_FLAG(info.device_caps, V4L2_CAP_VIDEO_OUTPUT_MPLANE);
    PRINT_FLAG(info.device_caps, V4L2_CAP_VIDEO_M2M);
    PRINT_FLAG(info.device_caps, V4L2_CAP_VIDEO_M2M_MPLANE);
    PRINT_FLAG(info.device_caps, V4L2_CAP_VIDEO_OVERLAY);
    PRINT_FLAG(info.device_caps, V4L2_CAP_VIDEO_OUTPUT_OVERLAY);
    PRINT_FLAG(info.device_caps, V4L2_CAP_VBI_CAPTURE);
    PRINT_FLAG(info.device_caps, V4L2_CAP_VBI_OUTPUT);
    PRINT_FLAG(info.device_caps, V4L2_CAP_SLICED_VBI_CAPTURE);
    PRINT_FLAG(info.device_caps, V4L2_CAP_SLICED_VBI_OUTPUT);
    PRINT_FLAG(info.device_caps, V4L2_CAP_RDS_CAPTURE);
    PRINT_FLAG(info.device_caps, V4L2_CAP_RDS_OUTPUT);
    PRINT_FLAG(info.device_caps, V4L2_CAP_HW_FREQ_SEEK);
    
    PRINT_FLAG(info.device_caps, V4L2_CAP_TUNER);
    PRINT_FLAG(info.device_caps, V4L2_CAP_AUDIO);
    PRINT_FLAG(info.device_caps, V4L2_CAP_RADIO);
    PRINT_FLAG(info.device_caps, V4L2_CAP_MODULATOR);
    PRINT_FLAG(info.device_caps, V4L2_CAP_SDR_CAPTURE);

    PRINT_FLAG(info.device_caps, V4L2_CAP_READWRITE);
    PRINT_FLAG(info.device_caps, V4L2_CAP_ASYNCIO);
    PRINT_FLAG(info.device_caps, V4L2_CAP_STREAMING);
    PRINT_FLAG(info.device_caps, V4L2_CAP_DEVICE_CAPS);
}

#define PRINT_FMT(D, X, Y) \
    if(X.pixelformat == Y) { \
        printf(#Y " (%s) %s\n", X.flags == V4L2_FMT_FLAG_COMPRESSED ? "compressed" : "normal", X.description); \
        print_enumframesizes(D, X.pixelformat); \
    }

void print_enumfmt(int dev) {
    
    struct v4l2_fmtdesc info;

    info.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    info.index = 0;

    printf("\n=====================================================================================\n");
    printf("ENUM_FMT / ENUM_FRAMESIZES / ENUM_FRAMEINTERVALS \n");
    printf("=====================================================================================\n");
 
    while(ioctl(dev, VIDIOC_ENUM_FMT, &info) >= 0) {        

        PRINT_FMT(dev, info, V4L2_PIX_FMT_RGB332);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_RGB444);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_RGB555);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_RGB565);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_RGB555X);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_RGB565X);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_BGR666);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_BGR24);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_RGB24);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_BGR32);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_RGB32);
        
        PRINT_FMT(dev, info, V4L2_PIX_FMT_GREY);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_Y4);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_Y6);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_Y10);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_Y12);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_Y16);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_Y10BPACK);
        
        PRINT_FMT(dev, info, V4L2_PIX_FMT_PAL8);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_UV8);
        
        PRINT_FMT(dev, info, V4L2_PIX_FMT_YVU410);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_YVU420);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_YUYV);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_YYUV);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_YVYU);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_UYVY);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_VYUY);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_YUV422P);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_YUV411P);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_Y41P);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_YUV444);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_YUV555);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_YUV565);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_YUV32);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_YUV410);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_YUV420);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_HI240);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_HM12);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_M420);
        
        PRINT_FMT(dev, info, V4L2_PIX_FMT_NV12);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_NV21);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_NV16);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_NV61);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_NV24);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_NV42);

        PRINT_FMT(dev, info, V4L2_PIX_FMT_NV12M);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_NV21M);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_NV16M);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_NV61M);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_NV12MT);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_NV12MT_16X16);

        PRINT_FMT(dev, info, V4L2_PIX_FMT_YUV420M);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_YVU420M);

        PRINT_FMT(dev, info, V4L2_PIX_FMT_SBGGR8);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_SGBRG8);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_SGRBG8);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_SRGGB8);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_SBGGR10);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_SGBRG10);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_SGRBG10);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_SRGGB10);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_SBGGR12);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_SGBRG12);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_SGRBG12);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_SRGGB12);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_SBGGR16);

        PRINT_FMT(dev, info, V4L2_PIX_FMT_SBGGR10ALAW8);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_SGBRG10ALAW8);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_SGRBG10ALAW8);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_SRGGB10ALAW8);

        PRINT_FMT(dev, info, V4L2_PIX_FMT_SBGGR10DPCM8);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_SGBRG10DPCM8);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_SGRBG10DPCM8);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_SRGGB10DPCM8);

        PRINT_FMT(dev, info, V4L2_PIX_FMT_MJPEG);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_JPEG);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_DV);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_MPEG);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_H264);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_H264_NO_SC);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_H264_MVC);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_H263);

        PRINT_FMT(dev, info, V4L2_PIX_FMT_MPEG1);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_MPEG2);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_MPEG4);
        
        PRINT_FMT(dev, info, V4L2_PIX_FMT_XVID);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_VC1_ANNEX_G);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_VC1_ANNEX_L);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_VP8);

        PRINT_FMT(dev, info, V4L2_PIX_FMT_CPIA1);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_WNVA);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_SN9C10X);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_SN9C20X_I420);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_PWC1);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_PWC2);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_ET61X251);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_SPCA501);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_SPCA505);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_SPCA508);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_SPCA561);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_PAC207);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_MR97310A);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_JL2005BCD);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_SN9C2028);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_SQ905C);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_PJPG);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_OV511);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_OV518);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_STV0680);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_TM6000);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_CIT_YYVYUY);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_KONICA420);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_JPGL);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_SE401);
        PRINT_FMT(dev, info, V4L2_PIX_FMT_S5C_UYVY_JPG);
        
        info.index++;       
    }   

    if(info.index == 0) {
        printf("None\n");
    }
}

void print_enumframesizes(int dev, int pixelformat)
{
    struct v4l2_frmsizeenum info;

    info.index = 0;
    info.pixel_format = pixelformat;

    while(ioctl(dev, VIDIOC_ENUM_FRAMESIZES, &info) >= 0) {
        if(info.type == V4L2_FRMSIZE_TYPE_DISCRETE) {
            int width = info.discrete.width;
            int height = info.discrete.height;

            printf("   DISCRETE  (%4d x %4d) ", width,height);

            struct v4l2_frmivalenum fps;

            fps.index = 0;
            fps.pixel_format = pixelformat;
            fps.width = width;
            fps.height = height;

            while(ioctl(dev, VIDIOC_ENUM_FRAMEINTERVALS, &fps) >= 0) {
                if(fps.type == V4L2_FRMIVAL_TYPE_DISCRETE) {
                    printf("%5.2lf fps ", fps.discrete.denominator / (double) fps.discrete.numerator);
                }

                fps.index++;
            }

            printf("\n");
        } 

        info.index++;
    }
}


void print_enumerate_menu(int dev, struct v4l2_queryctrl* ctrl) 
{
    struct v4l2_querymenu menu;
    memset(&menu, 0, sizeof(menu));

    menu.id = ctrl->id;

    printf(" [");

    for(menu.index = ctrl->minimum; menu.index <= ctrl->maximum; menu.index++) {
        if(ioctl(dev, VIDIOC_QUERYMENU, &menu) >= 0) {
            printf(" %u = '%s'", menu.index, menu.name);
        }
    }

    printf(" ]");
}



void print_queryctrl_in(int dev, int beg, int end)
{
    struct v4l2_queryctrl queryctrl;
    memset(&queryctrl, 0, sizeof(queryctrl));

    while(queryctrl.id < end) {
        memset(&queryctrl, 0, sizeof(queryctrl));
        
        queryctrl.id = beg;

        if(ioctl(dev, VIDIOC_QUERYCTRL, &queryctrl) < 0) {
            beg++;
            continue;
        }
        
        if(queryctrl.flags & V4L2_CTRL_FLAG_DISABLED)
            continue;

        const char* x = "%12s";

        switch(queryctrl.type) {
            case V4L2_CTRL_TYPE_INTEGER:
                printf(x, "INTEGER");
                break;

            case V4L2_CTRL_TYPE_BOOLEAN:
                printf(x, "BOOL");
                break;

            case V4L2_CTRL_TYPE_MENU:
                printf(x, "MENU");
                break;

            case V4L2_CTRL_TYPE_INTEGER_MENU:
                printf(x, "INTEGER_MENU");
                break;

            case V4L2_CTRL_TYPE_BITMASK:
                printf(x, "BITMASK");
                break;

            case V4L2_CTRL_TYPE_BUTTON:
                printf(x, "BUTTON");
                break;

            case V4L2_CTRL_TYPE_INTEGER64:
                printf(x, "INTEGER64");
                break;

            case V4L2_CTRL_TYPE_STRING:
                printf(x, "STRING");
                break;
                
            case V4L2_CTRL_TYPE_CTRL_CLASS:
                printf(x, "CTRL_CLASS");
                break;
       }

       printf(" %10d ", queryctrl.minimum);
       printf("%10d ", queryctrl.maximum);
       printf("%10d ", queryctrl.step);

        if(queryctrl.flags & V4L2_CTRL_FLAG_GRABBED)
            printf(" GR ");
        else 
            printf(" -- ");

        if(queryctrl.flags & V4L2_CTRL_FLAG_READ_ONLY)
            printf(" RD ");
        else 
            printf(" -- ");

        if(queryctrl.flags & V4L2_CTRL_FLAG_WRITE_ONLY)
            printf(" WR ");
        else 
            printf(" -- ");

        if(queryctrl.flags & V4L2_CTRL_FLAG_UPDATE)
            printf(" UP ");
        else 
            printf(" -- ");

        if(queryctrl.flags & V4L2_CTRL_FLAG_INACTIVE)
            printf(" IN ");
        else 
            printf(" -- ");

        if(queryctrl.flags & V4L2_CTRL_FLAG_VOLATILE)
            printf(" VO ");
        else 
            printf(" -- ");

        printf(" %30s", queryctrl.name);

        if(queryctrl.type == V4L2_CTRL_TYPE_MENU)
            print_enumerate_menu(dev, &queryctrl);

        printf("\n");
     
        beg++;
    }


}

void print_queryctrl(int dev)
{
    printf("\n=====================================================================================\n");
    printf("QUERYCTRL \n");
    printf("=====================================================================================\n");
 
    print_queryctrl_in(dev, V4L2_CID_BASE, V4L2_CID_FLASH_READY);
}


void print_querycrop(int dev)
{
    printf("\n=====================================================================================\n");
    printf("QUERYCROP \n");
    printf("=====================================================================================\n");

    struct v4l2_cropcap info;

    info.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if(ioctl(dev, VIDIOC_CROPCAP, &info) >= 0) {
        printf("Bounds:       (%5d,%5d) with width %5u, height %5u\n", info.bounds.left, info.bounds.top, info.bounds.width,info.bounds.height);
        printf("Default:      (%5d,%5d) with width %5u, height %5u\n", info.defrect.left, info.defrect.top, info.defrect.width,info.defrect.height);
        printf("Pixel aspect: %d/%d\n", info.pixelaspect.numerator, info.pixelaspect.denominator);
    } else {
        printf("No cropping capabilities\n");
    }
}

