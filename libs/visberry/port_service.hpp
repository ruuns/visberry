#pragma once

#include <string>
#include <memory>
#include <map>
#include <functional>
#include <boost/asio.hpp>
#include <rapidjson/document.h>

namespace visberry {


struct port_service {
    using socket_ptr    = std::shared_ptr<boost::asio::ip::tcp::socket>;
    using message_func  = std::function<std::string (socket_ptr, const rapidjson::Document&)>;

    const static int DefaultTcpPort { 2000 };

    port_service(const std::string& name, int tcp_port = DefaultTcpPort);
    ~port_service();

    std::string  service_name() const;
    std::string  tcp_address() const;
    int          tcp_port() const;

    void accept_async();
    void listen();

    void set_message_dispatch(const std::string& req, message_func fun);

private:
    struct message_description {
        std::string brief_text;
        std::string details;
        int arguments;
    };
    
    bool on_accept(socket_ptr socket);

private:
    boost::asio::io_service m_io_service;
    boost::asio::ip::tcp::endpoint m_tcp_endpoint;
    boost::asio::ip::tcp::acceptor m_tcp_acceptor;

    std::string m_service_name;

    std::map<std::string, message_func> m_dispatch_map;
    std::map<std::string, message_description> m_description_map;
};

}
