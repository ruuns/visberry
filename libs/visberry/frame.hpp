#pragma once

#include <cstdint>
#include <array>
#include <boost/asio.hpp>

namespace visberry {

static const char* kFrameVersion = "0.1";

struct frame_header {
    std::uint32_t  index;
    std::uint64_t  timestamp_captured;
    std::uint64_t  timestamp_received;

    std::uint32_t  width;
    std::uint32_t  height;
    std::uint32_t  bytes_per_line;
    std::uint8_t   pixelformat[4];

    std::size_t  size_max;
    std::size_t  size_current;
};


struct frame {
    frame_header  header;
    std::uint8_t* data;
};



} // visberry
