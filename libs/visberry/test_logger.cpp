#include "log.hpp"

int main(int argc, char** argv)
{
    LOG(info) << "Hello World in Info";
    LOG(debug) << "Hello World in Debug";
    LOG(fatal) << "Hello World in Fatal";

    return 0;
}
