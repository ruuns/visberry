#pragma once

#include <functional>
#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>

namespace visberry {
namespace json {

using writer_t = rapidjson::Writer<rapidjson::StringBuffer>;

std::string create_success();
std::string create_error(const std::string& msg = "");

std::string create_success_reply(const std::string& request, std::function<void (writer_t&)> fun = nullptr);
std::string create_error_reply(const std::string& request, const std::string& msg = "", std::function<void (writer_t&)> fun = nullptr);

void success(writer_t& writer);
void failure(writer_t& writer, const std::string& msg = "");
void message(writer_t& writer, const std::string& msg);
void request(writer_t& writer, const std::string& req);


void add_string (writer_t& writer, const std::string& key, const std::string& value);
void add_bool   (writer_t& writer, const std::string& key, bool value);
void add_int    (writer_t& writer, const std::string& key, int value);
void add_int64  (writer_t& writer, const std::string& key, std::int64_t value);
void add_uint   (writer_t& writer, const std::string& key, unsigned int value);
void add_uint64 (writer_t& writer, const std::string& key, std::uint64_t value);
void add_double (writer_t& writer, const std::string& key, double value);
void add_null   (writer_t& writer, const std::string& key);


}} // namespace visberry::json
