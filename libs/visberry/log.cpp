#include "log.hpp"

#include <boost/log/expressions.hpp>
#include <boost/log/sinks.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/attributes/clock.hpp>
#include <boost/log/attributes/current_thread_id.hpp>
#include <boost/log/expressions.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/core/null_deleter.hpp>
#include <boost/algorithm/string.hpp>
#include <ios>
#include <iomanip>

namespace attrs = boost::log::attributes;
namespace sinks = boost::log::sinks;
namespace exprs = boost::log::expressions;
namespace sources = boost::log::sources;
namespace logging = boost::log;
namespace keywords = boost::log::keywords;

BOOST_LOG_ATTRIBUTE_KEYWORD(timestamp, "TimeStamp", boost::posix_time::ptime);

BOOST_LOG_GLOBAL_LOGGER_INIT(logger, sources::severity_logger_mt<logging::trivial::severity_level>) 
{
    using text_sink = sinks::synchronous_sink<sinks::text_ostream_backend>;        

    sources::severity_logger_mt<logging::trivial::severity_level> logger;

    logger.add_attribute("TimeStamp", attrs::local_clock());

    boost::shared_ptr<text_sink> sink(boost::make_shared<text_sink>());
    boost::shared_ptr<std::ostream> stream(&std::clog, boost::null_deleter());

    sink->locked_backend()->add_stream(stream);
    sink->set_formatter( 
        exprs::stream 
            << std::setw(8) << std::setfill(' ') << logging::trivial::severity
            << exprs::format_date_time(timestamp, "  %Y-%m-%d %H:%M:%S  ")
            << exprs::smessage 
    );
    
    logging::core::get()->add_sink(sink);       

    return logger;
}

namespace visberry {

boost::optional<log_level> get_log_level_from_string(const std::string& token)
{
    if (boost::algorithm::iequals(token, "trace"))
        return boost::log::trivial::trace;
    
    else if (boost::algorithm::iequals(token, "debug"))
        return boost::log::trivial::debug;

    else if (boost::algorithm::iequals(token, "info"))
        return boost::log::trivial::info;

    else if (boost::algorithm::iequals(token, "warning"))
        return boost::log::trivial::warning;

    else if (boost::algorithm::iequals(token, "error"))
        return boost::log::trivial::error;

    else if (boost::algorithm::iequals(token, "fatal"))
        return boost::log::trivial::fatal;

    return boost::none;
}
} // namespace visberry

std::istream& operator>>(std::istream& in, visberry::log_level& level)
{
    std::string token;
    in >> token;

    auto log_level(visberry::get_log_level_from_string(token));

    if (log_level) 
        level = *log_level;
    else {
        in.setstate(std::ios::failbit);

        if (in.exceptions() & std::istream::failbit)
            throw std::istream::failure("No valid string representation for a visberry::log_level found in input stream");
    }
    
    return in;
}
