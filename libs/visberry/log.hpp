#pragma once

#include <boost/log/sources/global_logger_storage.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup.hpp>
#include <boost/optional.hpp>

#define LOG_LEVEL(level) boost::log::trivial::level
#define LOG(level) BOOST_LOG_SEV(logger::get(), boost::log::trivial::level)
#define LOG_SCOPE(str) BOOST_LOG_NAMED_SCOPE(str)

namespace visberry {
    using log_level = boost::log::trivial::severity_level;

    inline void set_log_level(log_level level) {
        boost::log::core::get()->set_filter(boost::log::trivial::severity >= level);
    }

    boost::optional<log_level> get_log_level_from_string(const std::string& level);
}

std::istream& operator>>(std::istream& in, visberry::log_level& level);

BOOST_LOG_GLOBAL_LOGGER(logger, boost::log::sources::severity_logger_mt<boost::log::trivial::severity_level>);


