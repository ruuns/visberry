#pragma once 

#include <algorithm>
#include <boost/endian/arithmetic.hpp>
#include <cstdint>

namespace visberry {

// --------------------------------------------------------------------------------------
// copy to byte buffer
// --------------------------------------------------------------------------------------

template <typename T>
inline void copy_to(std::vector<uint8_t>& ar, const T& field, std::size_t offset = 0) {
    std::copy(reinterpret_cast<const std::uint8_t*>(&field), reinterpret_cast<const std::uint8_t*>(&field + sizeof(field)), ar.data() + offset);
}

template <>
inline void copy_to<std::uint32_t>(std::vector<uint8_t>& ar, const std::uint32_t& field, std::size_t offset) {
    std::uint32_t field_network(boost::endian::native_to_big(field));

    std::copy(reinterpret_cast<const std::uint8_t*>(&field_network), reinterpret_cast<const std::uint8_t*>(&field_network + sizeof(field_network)), ar.data() + offset);
}

template <>
inline void copy_to<std::uint64_t>(std::vector<uint8_t>& ar, const std::uint64_t& field, std::size_t offset) {
    std::uint64_t field_network(boost::endian::native_to_big(field));

    std::copy(reinterpret_cast<const std::uint8_t*>(&field_network), reinterpret_cast<const std::uint8_t*>(&field_network + sizeof(field_network)), ar.data() + offset);
}

template <>
inline void copy_to<std::int32_t>(std::vector<uint8_t>& ar, const std::int32_t& field, std::size_t offset) {
    std::int32_t field_network(boost::endian::native_to_big(field));

    std::copy(reinterpret_cast<const std::uint8_t*>(&field_network), reinterpret_cast<const std::uint8_t*>(&field_network + sizeof(field_network)), ar.data() + offset);
}

template <>
inline void copy_to<std::int64_t>(std::vector<uint8_t>& ar, const std::int64_t& field, std::size_t offset) {
    std::int64_t field_network(boost::endian::native_to_big(field));

    std::copy(reinterpret_cast<const std::uint8_t*>(&field_network), reinterpret_cast<const std::uint8_t*>(&field_network + sizeof(field_network)), ar.data() + offset);
}


inline void copy_string_to(std::vector<uint8_t>& ar, const std::uint8_t* string, std::size_t len, std::size_t offset) {
    std::copy(string, string + len, ar.data() + offset);
}

inline void copy_string_to(std::vector<uint8_t>& ar, const std::int8_t* string, std::size_t len, std::size_t offset) {
    copy_string_to(ar, reinterpret_cast<const std::uint8_t*>(string), len, offset);
}


// --------------------------------------------------------------------------------------
// copy from byte buffer
// --------------------------------------------------------------------------------------

template <typename T>
inline void copy_from(const std::vector<uint8_t>& ar, T& field, std::size_t offset = 0) {
    field = *reinterpret_cast<const T*>(ar.data() + offset);
}

template <>
inline void copy_from<std::uint32_t>(const std::vector<uint8_t>& ar, std::uint32_t& field, std::size_t offset) {
    field = boost::endian::big_to_native(*reinterpret_cast<const std::uint32_t*>(ar.data() + offset));
}

template <>
inline void copy_from<std::int32_t>(const std::vector<uint8_t>& ar, std::int32_t& field, std::size_t offset) {
    field = boost::endian::big_to_native(*reinterpret_cast<const std::int32_t*>(ar.data() + offset));
}

template <>
inline void copy_from<std::uint64_t>(const std::vector<uint8_t>& ar, std::uint64_t& field, std::size_t offset) {
    field = boost::endian::big_to_native(*reinterpret_cast<const std::uint64_t*>(ar.data() + offset));
}

template <>
inline void copy_from<std::int64_t>(const std::vector<uint8_t>& ar, std::int64_t& field, std::size_t offset) {
    field = boost::endian::big_to_native(*reinterpret_cast<const std::int64_t*>(ar.data() + offset));
}


inline void copy_string_from(const std::vector<uint8_t>& ar, std::uint8_t* string, std::size_t len, std::size_t offset) {
    std::copy(ar.data() + offset, ar.data() + offset + len, string); 
}

inline void copy_string_from(const std::vector<uint8_t>& ar, std::int8_t* string, std::size_t len, std::size_t offset) {
    copy_string_from(ar, reinterpret_cast<std::int8_t*>(string), len, offset);
}






} // namespace visberry
