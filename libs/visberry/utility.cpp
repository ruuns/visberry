#include "utility.hpp"

#include <cstdint>

namespace visberry {

bool    
is_little_endian () 
{
    volatile std::uint32_t n = 0x01234567;
    return *((std::uint8_t*) &n) == 0x67;
}

} // namespace visberry
