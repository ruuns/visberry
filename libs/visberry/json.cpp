#include "json.hpp"
#include <ctime>
#include <time.h>

using namespace rapidjson;

namespace visberry {
namespace json {

inline void success(writer_t& w)
{
    w.Key("status");
    w.String("success");
    
    message(w, "");
}

inline void failure(writer_t& w, const std::string& msg)
{
    w.Key("status");
    w.String("failed");

    message(w, msg);
}

inline void message(writer_t& w, const std::string& msg)
{
    w.Key("message");
    w.String(msg.c_str(), msg.size());
}

inline void request(writer_t& w, const std::string& req)
{
    w.Key("request");
    w.String(req.c_str(), req.size());
}

inline void timestamp(writer_t& w)
{
    struct tm utc_now;
    std::time_t now(time(0));
    std::array<char, 32> f_utc_time;

    std::strftime(f_utc_time.data(), f_utc_time.size(), "%F %T %Z", gmtime_r(&now, &utc_now));

    w.Key("timestamp");
    w.String(f_utc_time.data());
}


std::string create_success()
{
    StringBuffer buffer;
    Writer<StringBuffer> w(buffer);

    w.StartObject();

    success(w);
    timestamp(w);

    w.EndObject();

    return std::string(buffer.GetString(), buffer.GetSize());
}


std::string create_error(const std::string& msg)
{
    StringBuffer buffer;
    Writer<StringBuffer> w(buffer);

    w.StartObject();

    failure(w, msg);
    timestamp(w);

    w.EndObject();

    return std::string(buffer.GetString(), buffer.GetSize());
}



std::string create_success_reply(const std::string& req, std::function<void (writer_t&)> fun)
{
    StringBuffer buffer;
    Writer<StringBuffer> w(buffer);

    w.StartObject();

    success(w);
    timestamp(w);
    request(w, req);

    if (fun) {
        w.Key("reply");
        w.StartObject();
        fun(w);
        w.EndObject();
    }

    w.EndObject();

    return std::string(buffer.GetString(), buffer.GetSize());
}




std::string create_error_reply(const std::string& req, const std::string& msg, std::function<void (writer_t&)> fun) 
{
    StringBuffer buffer;
    Writer<StringBuffer> w(buffer);

    w.StartObject();

    failure(w, msg);
    timestamp(w);
    request(w, req);

    if (fun) {
        w.Key("reply");
        w.StartObject();
        fun(w);
        w.EndObject();
    }

    w.EndObject();

    return std::string(buffer.GetString(), buffer.GetSize());
}



void add_string(writer_t& writer, const std::string& key, const std::string& value)
{
    writer.Key(key.c_str(), key.size());
    writer.String(value.c_str(), value.size());
}

void add_bool(writer_t& writer, const std::string& key, bool value)
{
    writer.Key(key.c_str(), key.size());
    writer.Bool(value);
}

void add_int(writer_t& writer, const std::string& key, int value)
{
    writer.Key(key.c_str(), key.size());
    writer.Int(value);
}

void add_int64(writer_t& writer, const std::string& key, std::int64_t value)
{
    writer.Key(key.c_str(), key.size());
    writer.Int64(value);
}

void add_uint(writer_t& writer, const std::string& key, unsigned int value)
{
    writer.Key(key.c_str(), key.size());
    writer.Uint(value);
}

void add_uint64(writer_t& writer, const std::string& key, std::uint64_t value)
{
    writer.Key(key.c_str(), key.size());
    writer.Uint64(value);
}

void add_double(writer_t& writer, const std::string& key, double value)
{
    writer.Key(key.c_str(), key.size());
    writer.Double(value);
}

void add_null(writer_t& writer, const std::string& key)
{
    writer.Key(key.c_str(), key.size());
    writer.Null();
}



}} // namespace visberry::json
