#include "port_service.hpp"
#include "log.hpp"
#include "messages.hpp"
#include <rapidjson/error/en.h>

using namespace boost::asio;
using namespace boost::asio::ip;

namespace visberry {

const int port_service::DefaultTcpPort;

port_service::port_service(const std::string& name, int tcp_port) 
    : m_tcp_endpoint(tcp::v4(), tcp_port)
    , m_tcp_acceptor(m_io_service, m_tcp_endpoint)
    , m_service_name(name)
{
    m_tcp_acceptor.listen();
}


port_service::~port_service()
{
}


std::string 
port_service::service_name() const 
{
    return m_service_name;
}


std::string
port_service::tcp_address() const
{
    return m_tcp_endpoint.address().to_string();
}


int
port_service::tcp_port() const
{
    return m_tcp_endpoint.port();
}



void
port_service::accept_async()
{
    socket_ptr socket = std::make_shared<boost::asio::ip::tcp::socket>(m_io_service);

    m_tcp_acceptor.async_accept(*socket, [this, socket](const boost::system::error_code& ec) {
        this->accept_async();

        if (ec) 
        {
            LOG(error) << "Could not accept asynchronously an incoming connection: " << ec.message();
        } 
        else 
        {
            if (on_accept(socket))
                m_io_service.stop();
        }
    });
}



bool port_service::on_accept(port_service::socket_ptr socket) 
{
    LOG(debug) << "Accepting a new tcp connection from " << socket->remote_endpoint().address();

    try {
        /* Reading data from client */
        std::string request;

        std::int32_t size(0);
        boost::asio::read(*socket, boost::asio::buffer(&size, sizeof(size)));

        std::vector<char> buffer(size);
        boost::asio::read(*socket, boost::asio::buffer(buffer.data(), buffer.size()));

        buffer.push_back('\0');

        rapidjson::Document doc;

        if (doc.Parse(buffer.data()).HasParseError()) {
            std::stringstream ss;
            ss << "Parsing error found in incoming JSON request from " << socket->remote_endpoint().address() << " : " << rapidjson::GetParseError_En(doc.GetParseError()) << std::endl;

            std::string msg(ss.str());
            LOG(error) << msg;
            request = json::create_error_reply(msg);

        } else if (!doc.HasMember("request") || !doc["request"].IsString()) {
            std::stringstream ss;
            ss << "JSON request from " << socket->remote_endpoint().address() << " has no proper request string";

            std::string msg(ss.str());
            LOG(error) << msg;
            request = json::create_error_reply(msg);

        } else {
            auto req_str = doc["request"].GetString();
            auto it = m_dispatch_map.find(req_str);

            if (it != m_dispatch_map.end()) {
                request = (it->second)(socket, doc);
            } else {
                std::stringstream ss;
                ss << "JSON request '" << req_str << "' is not supported by this webcam service";
                
                request = json::create_error_reply(ss.str());
            }
        }

        // if service returns an empty string no reply should be sent
        if (!request.empty()) {
            size = request.size();

            boost::asio::write(*socket, boost::asio::buffer(&size, sizeof(size)));
            boost::asio::write(*socket, boost::asio::buffer(request.c_str(), request.size()));
        }

    } catch (boost::system::system_error& e) {
        LOG(error) << "TCP communication problem occured : " << e.what();
    }

    socket->close();

    return false;
}


void port_service::set_message_dispatch(const std::string& req, message_func fun)
{
    m_dispatch_map[req] = fun;
}


void
port_service::listen()
{
    m_io_service.run();
}



}

