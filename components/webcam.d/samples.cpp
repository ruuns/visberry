#include "samples.hpp"
#include <boost/assert.hpp>
#include <visberry/network_conversion.hpp>

namespace visberry {
namespace webcamd {

/*
inline void serialize(std::vector<std::uint8_t>& ar, const net_header& header) {
    BOOST_ASSERT_MSG(ar.size() >= header.size(), "byte buffer has not enough space to serialize header informations");

    copy_to(ar, header.type, 0);
    copy_to(ar, header.index, sizeof(header.type));
    copy_to(ar, header.timestamp_created, sizeof(header.type) + sizeof(header.index)); 
    copy_to(ar, header.timestamp_received, sizeof(header.type) + sizeof(header.index) + sizeof(header.timestamp_created)); 
    copy_to(ar, header.body_size, sizeof(header.type) + sizeof(header.index) + sizeof(header.timestamp_created) * 2);
}


void serialize(std::vector<std::uint8_t>& ar, const net_info& info) {
    BOOST_ASSERT_MSG(ar.size() >= info.size(), "byte buffer has not enough space to serialize an info message");

    std::size_t device_offset(sizeof(info.protocol_version) + sizeof(info.webcam_name) + sizeof(info.webcam_device));
    std::size_t property_offset(device_offset + sizeof(info.width) + sizeof(info.height) + sizeof(info.bytes_per_line) + sizeof(info.pixelformat) + sizeof(info.buffer_size));

    // serialize webcam device info to byte buffer
    copy_string_to(ar, info.protocol_version, sizeof(info.protocol_version), 0);
    copy_string_to(ar, info.webcam_name, sizeof(info.webcam_name), sizeof(info.protocol_version));
    copy_string_to(ar, info.webcam_device, sizeof(info.webcam_device), sizeof(info.protocol_version) + sizeof(info.webcam_name));

    // serialize pixel format info to byte buffer
    copy_to(ar, info.width, device_offset);
    copy_to(ar, info.height, device_offset + sizeof(info.width));
    copy_to(ar, info.bytes_per_line, device_offset + sizeof(info.width) + sizeof(info.height));
    copy_string_to(ar, info.pixelformat, sizeof(info.pixelformat), device_offset + sizeof(info.width) + sizeof(info.height) + sizeof(info.bytes_per_line));
    copy_to(ar, info.buffer_size, device_offset + sizeof(info.width) + sizeof(info.height) + sizeof(info.bytes_per_line) + sizeof(info.pixelformat));

    // serialize implicit property size to byte buffer
    std::uint32_t num_properties(info.properties.size());
    copy_to(ar, num_properties, property_offset);  

    // serialize properties to byte buffer
    std::size_t offset(property_offset + sizeof(std::uint32_t));
    for(const property_t& property : info.properties) {
        copy_to(ar, property.first, offset);
        copy_to(ar, property.second, offset + sizeof(property.first));
        
        offset += sizeof(property.first) + sizeof(property.second);
    }
}

void serialize(std::vector<std::uint8_t>& ar, const net_property_update& update) {
    BOOST_ASSERT_MSG(ar.size() >= update.size(), "byte buffer has not enough space to serialize a property_update message");

    // serialize property update to byte_buffer
    copy_to(ar, update.property.first, 0);
    copy_to(ar, update.property.second, sizeof(update.property.first)); 
}


void serialize(std::vector<std::uint8_t>& ar, const net_frame& frame) {
    BOOST_ASSERT_MSG(ar.size() >= frame.size(), "byte buffer has not enough space to serialize a frame message");

    copy_to(ar, frame.data_size, 0);
    copy_string_to(ar, frame.data_ptr, frame.data_size, sizeof(frame.data_size));
}



void deserialize(const std::vector<std::uint8_t>& ar, net_header& header) {
    BOOST_ASSERT_MSG(ar.size() >= header.size(), "byte buffer has not enough space to deserialize header informations");

    copy_from(ar, header.type, 0);
    copy_from(ar, header.index, sizeof(header.type));
    copy_from(ar, header.timestamp_created, sizeof(header.type) + sizeof(header.index)); 
    copy_from(ar, header.timestamp_received, sizeof(header.type) + sizeof(header.index) + sizeof(header.timestamp_created)); 
    copy_from(ar, header.body_size, sizeof(header.type) + sizeof(header.index) + sizeof(header.timestamp_created) * 2);
}

void deserialize(const std::vector<std::uint8_t>& ar, net_info& info) {
    BOOST_ASSERT_MSG(ar.size() >= info.size(), "byte buffer has not enough space to serialize an info message");

    std::size_t device_offset(sizeof(info.protocol_version) + sizeof(info.webcam_name) + sizeof(info.webcam_device));
    std::size_t property_offset(device_offset + sizeof(info.width) + sizeof(info.height) + sizeof(info.bytes_per_line) + sizeof(info.pixelformat) + sizeof(info.buffer_size));

    // serialize webcam device info to byte buffer
    copy_string_from(ar, info.protocol_version, sizeof(info.protocol_version), 0);
    copy_string_from(ar, info.webcam_name, sizeof(info.webcam_name), sizeof(info.protocol_version));
    copy_string_from(ar, info.webcam_device, sizeof(info.webcam_device), sizeof(info.protocol_version) + sizeof(info.webcam_name));

    // serialize pixel format info to byte buffer
    copy_from(ar, info.width, device_offset);
    copy_from(ar, info.height, device_offset + sizeof(info.width));
    copy_from(ar, info.bytes_per_line, device_offset + sizeof(info.width) + sizeof(info.height));
    copy_string_from(ar, info.pixelformat, sizeof(info.pixelformat), device_offset + sizeof(info.width) + sizeof(info.height) + sizeof(info.bytes_per_line));
    copy_from(ar, info.buffer_size, device_offset + sizeof(info.width) + sizeof(info.height) + sizeof(info.bytes_per_line) + sizeof(info.pixelformat));

    // serialize implicit property size to byte buffer
    std::uint32_t num_properties(info.properties.size());
    copy_from(ar, num_properties, property_offset);  

    // serialize properties to byte buffer
    std::size_t offset(property_offset + sizeof(std::uint32_t));

    while (num_properties-- > 0) {
        property_t property; 

        copy_from(ar, property.first, offset);
        copy_from(ar, property.second, offset + sizeof(property.first));
        
        offset += sizeof(property.first) + sizeof(property.second);
    }

}

void deserialize(const std::vector<std::uint8_t>& ar, net_frame& frame) {
    BOOST_ASSERT_MSG(ar.size() >= frame.size(), "byte buffer has not enough space to serialize a property_update message");
    copy_from(ar, frame.data_size, 0);
    copy_string_from(ar, frame.data_ptr, frame.data_size, sizeof(frame.data_size));
}

void deserialize(const std::vector<std::uint8_t>& ar, net_property_update& update) {
    BOOST_ASSERT_MSG(ar.size() >= update.size(), "byte buffer has not enough space to serialize a property_update message");

    copy_from(ar, update.property.first, 0);
    copy_from(ar, update.property.second, sizeof(update.property.first));
}

*/


}} // namespace visberry::webcamd
