#pragma once

#include <functional>
#include <string>
#include <memory>
#include <mutex>
#include <vector>
#include <boost/asio.hpp>
#include <boost/property_tree/ptree.hpp>
#include <rapidjson/document.h>
#include <webcam-v4l2.h>

#ifdef __posix__
#include <time.h>
#endif

struct webcam_device;

namespace visberry {
   
// ------------------------------------------------------------------------------------------------
// webcam_daemon 
// ------------------------------------------------------------------------------------------------

struct webcam_daemon 
{
    using socket_ptr = std::shared_ptr<boost::asio::ip::tcp::socket>;

    webcam_daemon(const std::string& name, const std::string& configuration_file);
    ~webcam_daemon();

    bool open_device(const std::string& device_name);
    void close_device();
    void accept();
    void listen();
    void shutdown();
    bool process_connection(socket_ptr socket);
    void close_connections();
    bool load_configuration(const std::string& configuration_file = "");

private:
    using device_deleter = void (*)(struct webcam_device*);
    using configuration = boost::property_tree::ptree;
    using json_message_handler = std::function<std::string (socket_ptr, const rapidjson::Document&)>;

    static void write_image_to_sockets(uint32_t index, uint8_t* data, size_t size, uint64_t timestamp, struct webcam_format* fmt, void* daemon);

    std::string handle_json_status(socket_ptr, const rapidjson::Document&);
    std::string handle_json_get_format(socket_ptr, const rapidjson::Document&);
    std::string handle_json_get_controls(socket_ptr, const rapidjson::Document&);
    std::string handle_json_set_format(socket_ptr, const rapidjson::Document&);
    std::string handle_json_set_controls(socket_ptr, const rapidjson::Document&);
    std::string handle_json_start_capture(socket_ptr, const rapidjson::Document&);
    std::string handle_json_stop_capture(socket_ptr, const rapidjson::Document&);
    std::string handle_json_connect(socket_ptr, const rapidjson::Document&);
    std::string handle_json_shutdown(socket_ptr, const rapidjson::Document&);

    // deleted constructos and operators
    webcam_daemon(const webcam_daemon&) = delete;
    webcam_daemon& operator=(const webcam_daemon&) = delete;

private:
    // status daemon informations
    std::string m_daemon_name;
    std::time_t m_uptime;

    // daemon configuration
    std::string m_configuration_file;
    boost::property_tree::ptree m_configuration;

    // daemon network communication
    boost::asio::io_service m_io_service;
    boost::asio::signal_set m_signals;
    boost::asio::ip::tcp::endpoint m_tcp_endpoint;
    boost::asio::ip::tcp::acceptor m_tcp_acceptor;
   
    static std::array<std::int32_t, MAX_CONTROL_NUMBERS> m_controls;
    static std::vector<socket_ptr> m_sockets;
    static std::mutex m_socket_mutex;

    // daemon json interface
    std::map<std::string, json_message_handler> m_json_dispatcher; 

    // daemon camera interface
    std::string m_device_name;
    std::unique_ptr<webcam_device, device_deleter> m_device;
    bool m_device_autostart;
    int m_device_buffers;
};


}
