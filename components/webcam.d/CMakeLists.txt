cmake_minimum_required(VERSION 3.2)
project(webcam.d)

set(SOURCES
  main.cpp
  webcam_daemon.cpp)


add_executable(${PROJECT_NAME} ${SOURCES})
target_link_libraries(${PROJECT_NAME} visberry webcam-v4l2  ${BOOST_LIBRARIES})

install(TARGETS ${PROJECT_NAME} DESTINATION bin)

