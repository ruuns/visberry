#pragma once 

#include <vector>
#include <array>

namespace visberry {
namespace webcamd {

// ------------------------------------------------------------------------------
// enums
// ------------------------------------------------------------------------------

enum class sample : std::uint8_t {
    info,
    format,
    frame
};

enum class pixelformat : std::uint8_t {
    unknown,
    yuyv,
    mjpeg
};

enum class property : std::uint8_t {
    brightness,
    contrast,
    saturation,
    white_balance,
    gain,
    power_line_frequency,
    sharpness,
    backlight_compensation,
    zoom,
    pan,
    tilt,
    focus,
    exposure,
    fps,
    auto_exposure,
    auto_white_balance,
    auto_focus
};

struct header {
    sample type;
    std::uint32_t index;
    std::uint64_t timestamp_created;
    std::uint64_t timestamp_received;
    std::uint64_t body_size;
};

struct payload_device {
    std::uint8_t  protocol_version[4];
    std::int8_t   webcam_name[128];
    std::int8_t   webcam_device[128];
};

struct payload_format {
    std::uint32_t width;
    std::uint32_t height;
    std::uint32_t bytes_per_line;
    std::uint8_t  pixelformat[4];
    std::uint32_t buffer_size;
};

struct payload_data {
    std::vector<std::uint8_t> properties; 
    std::vector<std::uint8_t> frame;
};

struct payload_property {
    property key;
    int8_t name[64];
    int8_t description[128]; 
};

inline std::size_t msgsize(const header& h) {
    return sizeof(h.type) + sizeof(h.index) + sizeof(h.timestamp_created) + sizeof(h.timestamp_received) + sizeof(h.body_size);
}

inline std::size_t msgsize(const payload_device& d) {
    return sizeof(d.protocol_version) + sizeof(d.webcam_name) + sizeof(d.webcam_device);
}

inline std::size_t msgsize(const payload_format& f) {
    return sizeof(f.pixelformat) + sizeof(f.width) + sizeof(f.height) + sizeof(f.bytes_per_line) + sizeof(f.buffer_size);
}

inline std::size_t msgsize(const payload_property& p) {
    return sizeof(p.key) + sizeof(p.name) + sizeof(p.description);
}

inline std::size_t msgsize(const payload_data& d) {
    return sizeof(std::uint8_t) * d.frame.size() + sizeof(std::uint8_t) * d.properties.size();
}


}} // namespace visberry::webcamd
