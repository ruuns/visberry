#include "webcam_daemon.hpp"
#include "samples.hpp"
#include <iostream>
#include <cstdlib>
#include <boost/asio.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/format.hpp>
#include <visberry/log.hpp>
#include <visberry/json.hpp>
#include <rapidjson/error/en.h>

using namespace boost::asio;
using namespace boost::asio::ip;

namespace visberry {

namespace request {
static const std::string status("status");
static const std::string get_format("get_format");
static const std::string get_controls("get_controls");
static const std::string set_format("set_format");
static const std::string set_controls("set_controls");
static const std::string start_capture("start_capture");
static const std::string stop_capture("stop_capture");
static const std::string connect("connect");
static const std::string shutdown("shutdown");
}


std::array<std::int32_t, MAX_CONTROL_NUMBERS> webcam_daemon::m_controls;
std::vector<webcam_daemon::socket_ptr> webcam_daemon::m_sockets;
std::mutex webcam_daemon::m_socket_mutex;


void webcam_device_deleter(struct webcam_device* device) 
{
    if (device != nullptr) {
        webcam_device_close(device);
    }
}
    

webcam_daemon::webcam_daemon(const std::string& name, const std::string& configuration_file)
    : m_daemon_name(name)
    , m_uptime(0)
    , m_signals(m_io_service)
    , m_configuration_file(configuration_file)
//    , m_tcp_endpoint(tcp::v4(), cfg.)
    , m_tcp_acceptor(m_io_service)
    , m_device(nullptr, webcam_device_deleter)
    , m_device_autostart(false)
    , m_device_buffers(4)
{
    // signal handling
    m_signals.add(SIGINT);
    m_signals.add(SIGTERM);
    m_signals.async_wait(boost::bind(&webcam_daemon::shutdown, this));

    std::fill(m_controls.begin(), m_controls.end(), std::numeric_limits<std::int32_t>::max());

    // dispatch handling
    m_json_dispatcher[request::status] = std::bind(&webcam_daemon::handle_json_status, this, std::placeholders::_1, std::placeholders::_2);
    m_json_dispatcher[request::get_format] = std::bind(&webcam_daemon::handle_json_get_format, this, std::placeholders::_1, std::placeholders::_2);
    m_json_dispatcher[request::get_controls] = std::bind(&webcam_daemon::handle_json_get_controls, this, std::placeholders::_1, std::placeholders::_2);
    m_json_dispatcher[request::set_format] = std::bind(&webcam_daemon::handle_json_set_format, this, std::placeholders::_1, std::placeholders::_2);
    m_json_dispatcher[request::set_controls] = std::bind(&webcam_daemon::handle_json_set_controls, this, std::placeholders::_1, std::placeholders::_2);
    m_json_dispatcher[request::start_capture] = std::bind(&webcam_daemon::handle_json_start_capture, this, std::placeholders::_1, std::placeholders::_2);
    m_json_dispatcher[request::stop_capture] = std::bind(&webcam_daemon::handle_json_stop_capture, this, std::placeholders::_1, std::placeholders::_2);
    m_json_dispatcher[request::shutdown] = std::bind(&webcam_daemon::handle_json_shutdown, this, std::placeholders::_1, std::placeholders::_2);
}


webcam_daemon::~webcam_daemon()
{
}


bool webcam_daemon::load_configuration(const std::string& configuration_file)
{
    std::string file(!configuration_file.empty() ? configuration_file : m_configuration_file);

    if (!file.empty()) {
        try {
            boost::property_tree::read_json(file, m_configuration);

            LOG(info) << "Configuration loaded from " << file;
        } catch (const boost::property_tree::json_parser_error& e) {
            LOG(error) << "Could not load configuration: " << e.what(); 
            return false;
        }
    } else {
        LOG(info) << "No configuration file is given. Use default configuration";
    }

    bool use_ipv6(m_configuration.get("ipv6", false));
    int  tcp_port(m_configuration.get("tcp_port", 1100));
    std::string str_log_level(m_configuration.get("log_level", ""));

    if (!str_log_level.empty()) {
        auto log_level(visberry::get_log_level_from_string(str_log_level));

        if (log_level) 
            visberry::set_log_level(*log_level);
        else {
            LOG(error) << "Invalid log_level '" << str_log_level << "' used in configuration file " << file;
        }
    }

    m_tcp_endpoint = tcp::endpoint(use_ipv6 ? tcp::v6() : tcp::v4(), tcp_port);
    
    m_device_name = m_configuration.get("device", "");
    m_device_autostart = m_configuration.get("device_autostart", false);

    return true;
}


void webcam_daemon::accept()
{
    socket_ptr socket = std::make_shared<boost::asio::ip::tcp::socket>(m_io_service);
    
    m_tcp_acceptor.async_accept(*socket, [this, socket](const boost::system::error_code& ec) {
        this->accept();
        
        if (ec) {
            LOG(error) << "Could not accept asynchronous, incoming connection: " << ec.message();
            return;
        }
        
        if (process_connection(socket))
            this->shutdown();
    });
}


void webcam_daemon::listen()
{
    load_configuration();

    m_uptime = std::time(0);

    if (m_device_autostart)
        open_device(m_device_name);

    m_tcp_acceptor.open(m_tcp_endpoint.protocol());
    m_tcp_acceptor.bind(m_tcp_endpoint);
    m_tcp_acceptor.listen();

    LOG(info) << "Listening on " << m_tcp_endpoint;

    accept();
    
    m_io_service.run();
}

void webcam_daemon::shutdown()
{
    close_connections();
    close_device();

    LOG(info) << "Close all connections and shutdowns this daemon";

    m_tcp_acceptor.close();
    m_io_service.stop();
}

bool webcam_daemon::process_connection(socket_ptr socket)
{
    try {
        bool shutdown_daemon(false);
        std::uint32_t size(0);
        boost::asio::read(*socket, boost::asio::buffer(&size, sizeof(size)));

        std::vector<char> buffer(size);
        boost::asio::read(*socket, boost::asio::buffer(buffer.data(), buffer.size()));
        buffer.push_back('\0');

        rapidjson::Document doc;
        std::string request;


        if(doc.Parse(buffer.data()).HasParseError()) {
            std::stringstream ss;
            ss << "Parsing error found in incoming JSON request: " << rapidjson::GetParseError_En(doc.GetParseError());
            LOG(error) << ss.str() << " (from " << socket->remote_endpoint().address() << ")";
            request = json::create_error_reply(ss.str());
        } 
        else if (!doc.HasMember("request") || !doc["request"].IsString()) {
            std::stringstream ss;
            ss << "JSON request has no proper request field";
            LOG(error) << ss.str() << " (from " << socket->remote_endpoint().address() << ")";
            request = json::create_error_reply(ss.str());
        } 
        else {
            auto request_str(doc["request"].GetString());
            auto dit(m_json_dispatcher.find(request_str));

            if (dit != m_json_dispatcher.end()) {
                LOG(debug) << "Received new '" << request_str << "' request with " << size << " bytes (from " << socket->remote_endpoint().address() << ")";

                request = (dit->second)(socket, doc); 

                if (request_str == request::shutdown)
                    shutdown_daemon = true;

            } else {
                std::stringstream ss;
                ss << "JSON request '" << request_str << " is not supported by this webcam daemon";
                
                LOG(error) << ss.str() << " (from " << socket->remote_endpoint().address() << ")";
                request = json::create_error_reply(ss.str());
            }
        }

        if (!request.empty()) {
            size = request.size();

            boost::asio::write(*socket, boost::asio::buffer(&size, sizeof(size)));
            boost::asio::write(*socket, boost::asio::buffer(request.c_str(), request.size()));
        }

        if (shutdown_daemon)
            shutdown();

    } 
    catch(const boost::system::system_error& e) {
        LOG(error) << "TCP connection problem occured: " << e.what(); 
    }

    socket->close();

    return false;
}


bool webcam_daemon::open_device(const std::string& device_name) {
    if (m_device) {
        LOG(warning) << "Webcam device '" << m_device_name << "' is currently used. Can not open device '" << device_name << "'";
        return false;
    }

    webcam_device* device(webcam_device_open(device_name.c_str()));

    if (!device) {
        LOG(error) << "Webcam device '" << device_name << "' could not be opened"; 
        return false;
    }
 
    LOG(info) << "Webcam device '" << device_name << "' is opened now";
    m_device_name = device_name;
    m_device.reset(device);
    return true;
}


void webcam_daemon::close_device() {
    if (m_device) {
        m_device.reset(nullptr);
        LOG(info) << "Webcam device '" << m_device_name << "' closed";
    }
}


void webcam_daemon::close_connections() {
    std::lock_guard<std::mutex> lock(m_socket_mutex);

    for (socket_ptr& sock : m_sockets)
        sock->close();

    m_sockets.clear();
}


//-------------------------------------------------------------------------------------------------
// capture loop
//-------------------------------------------------------------------------------------------------

void webcam_daemon::write_image_to_sockets(uint32_t index, uint8_t* data, size_t size, uint64_t timestamp, struct webcam_format* fmt, void* hints)
{
    visberry::webcamd::header frame_header {
        visberry::webcamd::sample::frame, index, timestamp, 0, 
        m_controls.size() * sizeof(std::int32_t) + size * sizeof(std::uint8_t)
    };
   
    std::lock_guard<std::mutex>lock(m_socket_mutex);

    for (auto it(m_sockets.begin()); it != m_sockets.end();) {
        try {
            boost::asio::write(*(*it), boost::asio::buffer(&frame_header, sizeof(frame_header)));
            boost::asio::write(*(*it), boost::asio::buffer(m_controls));
            boost::asio::write(*(*it), boost::asio::buffer(data, size));
            ++it;
            continue;

        } catch (boost::system::system_error& e) {
            LOG(error) << "Remove client " << (*it)->remote_endpoint().address() << " from the capture loop due to: " << e.what();
            (*it)->close();
            it = m_sockets.erase(it);
        }
    }
}


//-------------------------------------------------------------------------------------------------
// connect_stream
//-------------------------------------------------------------------------------------------------

std::string webcam_daemon::handle_json_connect(socket_ptr socket, const rapidjson::Document& request) 
{
    if (!m_device)
        return json::create_error_reply(request::connect, "Webcam device is not opened");

    /*
    visberry::webcamd::header frame_header {
        visberry::webcamd::sample::frame, 0, timestamp, 0, 
        m_controls.size() * sizeof(std::int32_t) + size * sizeof(std::uint8_t)
    };
   

    

    boost::asio::write(*socket, boost::asio::buffer(&size, sizeof(size)));
    boost::asio::write(*socket, boost::asio::buffer(reply.c_str(), size));

    std::lock_guard<std::mutex>lock(m_socket_mutex);
    m_sockets.push_back(socket);

    */
    return "";
}


//-------------------------------------------------------------------------------------------------
// json :: status
//-------------------------------------------------------------------------------------------------

std::string webcam_daemon::handle_json_status(socket_ptr socket, const rapidjson::Document& req) {
    return json::create_success_reply(request::status, [this](json::writer_t& w) {
        // format server uptime to UTC string
        std::array<char, 32> f_utc_uptime;
        struct tm utc_uptime; 
        std::strftime(f_utc_uptime.data(), f_utc_uptime.size(), "%F %T %Z", gmtime_r(&m_uptime, &utc_uptime));

        json::add_string(w, "daemon_name", m_daemon_name);
        json::add_string(w, "configuration_file", m_configuration_file);
        json::add_string(w, "daemon_uptime", f_utc_uptime.data()); 
        json::add_uint64(w, "daemon_uptime_posix", m_uptime);
       
        // interface 
        w.Key("interface");
        w.StartArray();

        for (auto& kv : m_json_dispatcher)
            w.String(kv.first.c_str(), kv.first.size());
        
        w.EndArray();

        {
            std::lock_guard<std::mutex> lock(m_socket_mutex);

            // connected clients
            json::add_uint(w, "connected_clients", m_sockets.size());
            w.Key("connected_ips");
            w.StartArray();
            for (const auto& sockets : m_sockets) {
                auto ip(sockets->remote_endpoint().address().to_string());
                w.String(ip.c_str(), ip.size());
            }
            w.EndArray();
        }


        // device specific informations

        json::add_string(w, "device", m_device_name);
        json::add_int(w, "device_buffers", m_device_buffers);

        if (!m_device) {
            json::add_string(w, "device_status", "closed");
        } else {
            switch (webcam_device_get_capture_state(m_device.get())) {
                case RUNNING:
                    json::add_string(w, "device_status", "running");
                    break;

                case IDLE:
                    json::add_string(w, "device_status", "idle");
                    break;

                case ERROR_V4L2_QUEUE:
                    json::add_string(w, "device_status", "error_v4l2_queue");
                    break;

                case ERROR_V4L2_BUFFER:
                    json::add_string(w, "device_status", "error_v4l2_buffer");
                    break;

                case ERROR_V4L2_STREAMING:
                    json::add_string(w, "device_status", "error_v4l2_streaming");
                    break;

                case ERROR_V4L2:
                    json::add_string(w, "device_status", "error_v4l2");
                    break;
            }
        }

    });
}
 

//-------------------------------------------------------------------------------------------------
// json :: get_format
//-------------------------------------------------------------------------------------------------

std::string webcam_daemon::handle_json_get_format(socket_ptr socket, const rapidjson::Document& req) {
    webcam_format fmt;

    if (!m_device)
        return json::create_error_reply(request::get_format, "Webcam device is not opened");
    
    if (!webcam_device_get_pixelformat_details(this->m_device.get(), &fmt))
        return json::create_error_reply(request::get_format, "Could not retrieve current frame format settings for this webcam daemon");

    return json::create_success_reply(request::get_format, [this, &fmt](json::writer_t& writer) {
            json::add_int(writer, "width", fmt.width);
            json::add_int(writer, "height", fmt.height);
            json::add_int(writer, "bytes_per_line", fmt.bytes_per_line);
            json::add_int(writer, "max_size", fmt.max_size);

            switch (fmt.pixelformat) {
                case UNKNOWN: 
                    json::add_string(writer, "pixelformat", "UNKNOWN");
                    break;

                case YUYV:
                    json::add_string(writer, "pixelformat", "YUYV");
                    break;

                case MJPEG:
                    json::add_string(writer, "pixelformat", "MJPEG");
                    break;
            }
    });
}


//-------------------------------------------------------------------------------------------------
// json :: get_control
//-------------------------------------------------------------------------------------------------

inline bool get_control_value(webcam_device* dev, webcam_control flag, std::array<int, MAX_CONTROL_NUMBERS>& controls, bool success) {
    return success && webcam_control_get(dev, flag, &controls[flag]); 
}

inline void add_control_int(json::writer_t& w, const std::array<int, MAX_CONTROL_NUMBERS>& controls, const char* string, webcam_control flag) {
    if (controls[flag] != std::numeric_limits<int>::max())
        json::add_int(w, string, controls[flag]);
    else
        json::add_null(w, string);
}

std::string webcam_daemon::handle_json_get_controls(socket_ptr socket, const rapidjson::Document& req)
{
    if (!m_device)
        return json::create_error_reply(request::get_format, "Webcam device is not opened");

    bool success(true);
    webcam_device* dev(m_device.get());
    std::array<int, MAX_CONTROL_NUMBERS> controls;
    std::fill(controls.begin(), controls.end(), std::numeric_limits<int>::max());

    for (int i = 0; i < MAX_CONTROL_NUMBERS; ++i) 
        success = get_control_value(dev, static_cast<webcam_control>(i), controls, success);

    auto field_adder = [&controls](json::writer_t& w) {
        add_control_int(w, controls, "brightness", CONTROL_BRIGHTNESS);
        add_control_int(w, controls, "contrast", CONTROL_CONTRAST);
        add_control_int(w, controls, "saturation", CONTROL_SATURATION);
        add_control_int(w, controls, "white_balacne", CONTROL_WHITE_BALANCE);
        add_control_int(w, controls, "gain", CONTROL_GAIN);
        add_control_int(w, controls, "power_line_frequency", CONTROL_POWER_LINE_FREQUENCY);
        add_control_int(w, controls, "sharpness", CONTROL_SHARPNESS);
        add_control_int(w, controls, "backlight_compensation", CONTROL_BACKLIGHT_COMPENSATION);
        add_control_int(w, controls, "zoom", CONTROL_ZOOM);
        add_control_int(w, controls, "pan", CONTROL_PAN);
        add_control_int(w, controls, "tilt", CONTROL_TILT);
        add_control_int(w, controls, "focus", CONTROL_FOCUS);
        add_control_int(w, controls, "exposure", CONTROL_EXPOSURE);
        add_control_int(w, controls, "auto_exposure", CONTROL_AUTO_EXPOSURE);
        add_control_int(w, controls, "auto_white_balance", CONTROL_AUTO_WHITE_BALANCE);
        add_control_int(w, controls, "auto_focus", CONTROL_AUTO_FOCUS);
        add_control_int(w, controls, "fps", CONTROL_FPS);
    };

    if (success)
        return json::create_success_reply(request::get_controls, field_adder);
    else
        return json::create_error_reply(request::get_controls, "Could not retrieve one of the control values", field_adder);
}
          
//-------------------------------------------------------------------------------------------------
// json :: set_format
//-------------------------------------------------------------------------------------------------

std::string webcam_daemon::handle_json_set_format(socket_ptr socket, const rapidjson::Document& req) {
    if (!m_device)
        return json::create_error_reply(request::set_format, "Webcam device is not opened");

    if (webcam_device_get_capture_state(m_device.get()) == RUNNING)
        return json::create_error_reply(request::set_format, "Format can not be changed while capture loop is running");

    if (!req.HasMember("format") || !req["format"].IsObject()) 
        return json::create_error_reply(request::set_format, "Request does not contain a format member attribute");
    
    const rapidjson::Value& format(req["format"]);

    if (!format.HasMember("width") || !format.HasMember("height") || !format.HasMember("pixelformat")
     || !format["width"].IsInt() || !format["height"].IsInt() || !format["pixelformat"].IsString())
        return json::create_error_reply(request::set_format, "Format in request is not in a valid shape");

    int width(format["width"].GetInt());
    int height(format["height"].GetInt());
    const char* pixelformat(format["pixelformat"].GetString());

    webcam_pixelformat pf;

    if (std::strcmp(pixelformat, "MJPEG") == 0)
        pf = MJPEG;
    else if (std::strcmp(pixelformat, "YUYV") == 0)
        pf = YUYV;
    else 
        return json::create_error_reply(request::set_format, "chosen pixelformat is unknown");

    if (!webcam_device_set_pixelformat(this->m_device.get(), pf, width, height)) 
        return json::create_error_reply(request::set_format, "Device could not set the given pixelformat");
    else
        return json::create_success_reply(request::set_format);
}

//-------------------------------------------------------------------------------------------------
// json :: set_control
//-------------------------------------------------------------------------------------------------

inline void add_string_control(json::writer_t& w, const char* msg, bool success, int controls) {
    if (success)
        json::add_string(w, msg, controls != std::numeric_limits<int>::max() ? "success" : "not set");
    else
        json::add_string(w, msg, "failed");
}

std::string webcam_daemon::handle_json_set_controls(socket_ptr socket, const rapidjson::Document& req) {
    if (!m_device)
        return json::create_error_reply(request::set_controls, "Webcam device is not opened");

    if (!req.HasMember("controls") || !req["controls"].IsObject())
        return json::create_error_reply(request::set_controls, "request does not contain a valid controls member attribute (json object)");

    const rapidjson::Value& control(req["controls"]);
    std::string error_msg;
    webcam_control c;
   
    std::array<int, MAX_CONTROL_NUMBERS> controls;
    std::array<bool, MAX_CONTROL_NUMBERS> success;
    std::fill(controls.begin(), controls.end(), std::numeric_limits<int>::max());
    std::fill(success.begin(), success.end(), true);

    for(auto p = control.MemberBegin(); p != control.MemberEnd(); ++p) {
        if (std::strcmp(p->name.GetString(), "brightness") == 0)
            c = CONTROL_BRIGHTNESS;

        else if (std::strcmp(p->name.GetString(), "contrast") == 0)
            c = CONTROL_CONTRAST;

        else if (std::strcmp(p->name.GetString(), "saturation") == 0)
            c = CONTROL_SATURATION;

        else if (std::strcmp(p->name.GetString(), "white_balance") == 0)
            c = CONTROL_WHITE_BALANCE;

        else if (std::strcmp(p->name.GetString(), "gain") == 0)
            c = CONTROL_GAIN;

        else if (std::strcmp(p->name.GetString(), "power_line_frequency") == 0)
            c = CONTROL_POWER_LINE_FREQUENCY;

        else if (std::strcmp(p->name.GetString(), "sharpness") == 0)
            c = CONTROL_SHARPNESS;

        else if (std::strcmp(p->name.GetString(), "backlight_compenstation") == 0)
            c = CONTROL_BACKLIGHT_COMPENSATION;

        else if (std::strcmp(p->name.GetString(), "zoom") == 0)
            c = CONTROL_ZOOM;

        else if (std::strcmp(p->name.GetString(), "tilt") == 0)
            c = CONTROL_TILT;

        else if (std::strcmp(p->name.GetString(), "pan") == 0)
            c = CONTROL_PAN;

        else if (std::strcmp(p->name.GetString(), "focus") == 0)
            c = CONTROL_FOCUS;

        else if (std::strcmp(p->name.GetString(), "exposure") == 0)
            c = CONTROL_EXPOSURE;

        else if (std::strcmp(p->name.GetString(), "auto_focus") == 0)
            c = CONTROL_AUTO_FOCUS;

        else if (std::strcmp(p->name.GetString(), "auto_exposure") == 0)
            c = CONTROL_AUTO_EXPOSURE;

        else if (std::strcmp(p->name.GetString(), "auto_white_balance") == 0)
            c = CONTROL_AUTO_WHITE_BALANCE;        

        else if (std::strcmp(p->name.GetString(), "fps") == 0)
            c = CONTROL_FPS;        

        else {
           return json::create_error_reply(request::set_controls,  boost::str(boost::format("Control value '%s' is not supported") % p->name.GetString()));
        }

        if (p->value.IsBool()) 
            controls[c] = static_cast<int>(p->value.GetBool());
        else if (p->value.IsInt())
            controls[c] = p->value.GetInt();
        else 
            return json::create_error_reply(request::set_controls, boost::str(boost::format("Control value '%s' is not an integer or boolean") % p->name.GetString()));
    }

    bool is_error(false);

    for (int i(0); i < MAX_CONTROL_NUMBERS; ++i) {
        if (controls[i] == std::numeric_limits<int>::max())
            continue;
           
        if (!webcam_control_set(m_device.get(), static_cast<webcam_control>(i), controls[i])) {
            success[i] = false;
            is_error = true;
        }
    }

    auto control_adder = [&success, &controls](json::writer_t& w) {
        add_string_control(w, "brightness", success[CONTROL_BRIGHTNESS], controls[CONTROL_BRIGHTNESS]);
        add_string_control(w, "contrast", success[CONTROL_CONTRAST], controls[CONTROL_CONTRAST]);
        add_string_control(w, "saturation", success[CONTROL_SATURATION], controls[CONTROL_SATURATION]);
        add_string_control(w, "white_balance", success[CONTROL_WHITE_BALANCE], controls[CONTROL_WHITE_BALANCE]);
        add_string_control(w, "gain", success[CONTROL_GAIN], controls[CONTROL_GAIN]);
        add_string_control(w, "power_line_frequency", success[CONTROL_POWER_LINE_FREQUENCY], controls[CONTROL_POWER_LINE_FREQUENCY]);
        add_string_control(w, "sharpness", success[CONTROL_SHARPNESS], controls[CONTROL_SHARPNESS]);
        add_string_control(w, "backlight_compensation", success[CONTROL_BACKLIGHT_COMPENSATION], controls[CONTROL_BACKLIGHT_COMPENSATION]);
        add_string_control(w, "zoom", success[CONTROL_ZOOM], controls[CONTROL_ZOOM]);
        add_string_control(w, "tilt", success[CONTROL_TILT], controls[CONTROL_TILT]);
        add_string_control(w, "pan", success[CONTROL_PAN], controls[CONTROL_PAN]);
        add_string_control(w, "focus", success[CONTROL_FOCUS], controls[CONTROL_FOCUS]);
        add_string_control(w, "exposure", success[CONTROL_EXPOSURE], controls[CONTROL_EXPOSURE]);
        add_string_control(w, "auto_focus", success[CONTROL_AUTO_FOCUS], controls[CONTROL_AUTO_FOCUS]);
        add_string_control(w, "auto_exposure", success[CONTROL_AUTO_EXPOSURE], controls[CONTROL_AUTO_EXPOSURE]);
        add_string_control(w, "auto_white_balance", success[CONTROL_AUTO_WHITE_BALANCE], controls[CONTROL_AUTO_WHITE_BALANCE]);
        add_string_control(w, "fps", success[CONTROL_FPS], controls[CONTROL_FPS]);
    };
            
    return is_error 
        ? json::create_error_reply(request::set_controls, "Some controls are not successfully executed", control_adder)
        : json::create_success_reply(request::set_controls, control_adder);
            
}


//-------------------------------------------------------------------------------------------------
// start_capture
//-------------------------------------------------------------------------------------------------

std::string webcam_daemon::handle_json_start_capture(socket_ptr socket, const rapidjson::Document& request) 
{
    if (!m_device)
        return json::create_error_reply(request::start_capture, "Webcam device is not opened");

    if (webcam_device_get_capture_state(m_device.get()) == RUNNING)
        return json::create_error_reply(request::set_format, "Capture loop from the webcam device is already running");

    if (request.HasMember("buffers") && request["buffers"].IsInt())
        m_device_buffers = request["buffers"].GetInt();

    if (!webcam_device_start_capture(m_device.get(), m_device_buffers, webcam_daemon::write_image_to_sockets, nullptr))
        return json::create_error_reply(request::start_capture, "Could not start capture loop of this webcam device");

    LOG(info) << "Capture loop is started with " << m_device_buffers << " buffers";
    
    return json::create_success_reply(request::start_capture);
}


//-------------------------------------------------------------------------------------------------
// stop_capture
//-------------------------------------------------------------------------------------------------

std::string webcam_daemon::handle_json_stop_capture(socket_ptr socket, const rapidjson::Document& request) 
{
    if (!m_device)
        return json::create_error_reply(request::stop_capture, "Webcam device is not opened");

    if (webcam_device_get_capture_state(m_device.get()) != RUNNING)
        return json::create_error_reply(request::set_format, "Capture loop from the webcam device is not running");

    if (!webcam_device_stop_capture(m_device.get()))
        return json::create_error_reply(request::stop_capture, "Could not stop capture loop of this webcam device");

    close_connections();
    
    LOG(info) << "Capture loop is stopped";
    
    return json::create_success_reply(request::stop_capture);
}



//-------------------------------------------------------------------------------------------------
// json :: shutdown 
//-------------------------------------------------------------------------------------------------

std::string webcam_daemon::handle_json_shutdown(socket_ptr socket, const rapidjson::Document& req) {
    return json::create_success_reply(request::shutdown);
}


/*
//-------------------------------------------------------------------------------------------------
// open
//-------------------------------------------------------------------------------------------------
std::string webcam_daemon::open(port_daemon::socket_ptr socket, const rapidjson::Document& request)
{
    if (m_device)
        return json::create_error_reply(kRequestopen, "Webcam device is already opened");

    if (request.HasMember("device") && request["device"].IsString()) {
        webcam_device* device = webcam_device_open(request["device"].GetString());

        if (device) 
        {
            m_device.reset(device);
            LOG(info) << "Webcam device to " << request["device"].GetString() << " is open";
            return json::create_success_reply(kRequestopen);
        }

        return json::create_error_reply(kRequestopen, "Could not open webcam device on " + std::string(request["device"].GetString()));
    }

    return json::create_error_reply(kRequestopen, "Request has not a proper 'device' string attribute");
}


//-------------------------------------------------------------------------------------------------
// close
//-------------------------------------------------------------------------------------------------
std::string webcam_daemon::close(port_daemon::socket_ptr socket, const rapidjson::Document& request)
{
    if (m_device) 
    {
        m_device.reset(nullptr);

        return json::create_success_reply(kRequestClose);
    }

    return json::create_error_reply(kRequestClose, "webcam device is already closed");
}


*/


}

