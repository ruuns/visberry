#include <iostream>
#include <sstream>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <vector>
#include <string>

#include <boost/asio.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/program_options.hpp>
#include <visberry/json.hpp>
#include <rapidjson/document.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/filewritestream.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <cstdlib>

int json_shell(std::string ip, int tcp_port);

namespace boost_args = boost::program_options;


using namespace visberry;
using namespace boost::asio::ip;
using namespace boost;

int main (int argc, char**argv)
{
    std::stringstream ss;
    ss << "usage: " << argv[0] << " [options] -n NAME" << std::endl << std::endl;
    ss << "Supported options";


    boost_args::options_description desc(ss.str());
    desc.add_options()
        ("ip,n",    boost_args::value<std::string>()->default_value("127.0.0.1"), "Give this component a unique name")
        ("tcp,t",     boost_args::value<int>()->default_value(1100), "Set an asynchronous tcp socket component interproccess communication")
        ("help,h",    "produce this help description");

    boost_args::variables_map vm;

    try {
        boost_args::store(
                boost_args::command_line_parser( argc, argv )
                .options(desc)
                .style ( boost_args::command_line_style::unix_style
                       | boost_args::command_line_style::allow_long_disguise )
                .run(),
                vm);

        boost_args::notify(vm);

        if (vm.count("help")) {
            std::cout << desc << std::endl;
            return EXIT_SUCCESS;
        }

    } catch (const boost_args::error& err) {
        std::cerr << "error: " << err.what() << std::endl << std::endl;
        std::cerr << desc << std::endl;
        return EXIT_FAILURE;
    }

    return json_shell(vm["ip"].as<std::string>(), vm["tcp"].as<int>());
}


/*
std::string gen_json_request_close() {
    return json::create_request("close");
}


std::string gen_json_request_connect(const std::string& device_name)
{
   return json::create_request("connect", [device_name](json::writer_t& writer) {
        json::add_string(writer, "device", device_name);
    });
}*/


std::string prompt_request()
{
    std::vector<char> buffer;

    int indent(1);

    do {
        char* pline(buffer.empty() ? readline("json> ") : readline(""));
        std::string str(pline);
        std::free(pline);
        
        boost::trim(str);

        if (str.empty() || (buffer.empty() && str[0] != '{'))
            return "";

        auto p(str.begin() + 1);
        buffer.push_back('{');

        while (p < str.end()) {
            if (*p == '{') 
                ++indent;
            else if (*p == '}')
                --indent;

            buffer.push_back(*p);

            p = (indent > 0) ? p + 1 : str.end();
        }
    } while (indent > 0);

    return std::string(buffer.begin(), buffer.end());
}




void write_request_to_server(const std::string& req, const std::string& ip, int tcp_port)
{
    boost::asio::io_service io_service;

    try {
        boost::system::error_code err_code;

        tcp::socket sock(io_service);
        tcp::endpoint endpoint(address::from_string(ip), tcp_port);

        sock.connect(endpoint, err_code);

        if (err_code) {
            std::cerr << "Could not connect to the webcam compoment on " << ip << ":" << tcp_port << " : " << err_code.message() << std::endl;
            return;
        }


        std::uint32_t size(req.size());
        
        asio::write(sock, asio::buffer(&size, sizeof(size)));    
        asio::write(sock, asio::buffer(req.c_str(), size));

        asio::read(sock, asio::buffer(&size, sizeof(size)));
        std::vector<char> incoming(size);
        asio::read(sock, asio::buffer(incoming.data(), incoming.size()));

        std::string reply(incoming.data(), incoming.size());

        rapidjson::Document doc;
        char buffer[1024];
        rapidjson::FileWriteStream f(stdout, buffer, sizeof(buffer));
        rapidjson::PrettyWriter<rapidjson::FileWriteStream> writer(f);

        if (!doc.Parse(reply.c_str()).HasParseError()) {
            doc.Accept(writer);
        } else {
            std::cerr << "Could not correctly parse JSON reply" << std::endl;
        }


    } catch (system::system_error& e) {
        std::cerr << "Communikation problem occured : " << e.what() << std::endl;
    }
            
    std::cout << std::endl;        
}



int json_shell(std::string ip, int tcp_port)
{
    while (true) {
        std::string request(prompt_request());

        if (!request.empty()) 
        {
            add_history(request.c_str());
            write_request_to_server(request, ip, tcp_port);
        } 
        else 
        {
            break;
        }
    }

    return EXIT_SUCCESS;
}
