#pragma once

#include <string>
#include <boost/property_tree/ptree.hpp>

namespace visberry {

struct webcamlog_daemon {
    webcamlog_daemon(const std::string& configuration_file);
    ~webcamlog_daemon();
    
    void connect() {}
    bool load_configuration(const std::string& configuration_file = "") {}

private:
    webcamlog_daemon(const webcamlog_daemon&) = delete;
    webcamlog_daemon& operator=(const webcamlog_daemon&) = delete;

private:
    std::string m_configuration_file;
    boost::property_tree::ptree m_configuration;
};

} // namespace visberry

