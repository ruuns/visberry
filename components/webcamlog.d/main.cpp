#include <iostream>
#include <sstream>
#include <cstdlib>
#include <boost/property_tree/json_parser.hpp>
#include <boost/program_options.hpp>
#include <visberry/log.hpp>
#include "webcamlog_daemon.hpp"

using namespace visberry;
namespace boost_args = boost::program_options;

static const int DefaultTcpPort = 1100;

int main (int argc, char**argv)
{
    std::stringstream ss;
    ss << "usage: " << argv[0] << " [options] -c FILE" << std::endl << std::endl;
    ss << "Supported options";

    boost_args::options_description desc(ss.str());
    desc.add_options()
        ("config,c",   boost_args::value<std::string>(), "Use a given configuration file in JSON format")
        ("loglevel,l", boost_args::value<std::string>(), "Initial loglevel for this webcamlog daemon. Possible Values (trace, debug, info, warning, error, fatal)")
        ("help,h",    "produce this help description");

    boost_args::variables_map vm;
    std::string configuration_file;
    std::string name;
    visberry::log_level loglevel;

    try {
        boost_args::store(
                boost_args::command_line_parser( argc, argv )
                .options(desc)
                .style ( boost_args::command_line_style::unix_style
                       | boost_args::command_line_style::allow_long_disguise )
                .run(),
                vm);

        boost_args::notify(vm);

        if (vm.count("help")) {
            std::cout << desc << std::endl;
            return EXIT_SUCCESS;
        }
 
        if (vm.count("config"))
            configuration_file = vm["config"].as<std::string>();

        if (vm.count("loglevel")) {
            auto log(visberry::get_log_level_from_string(vm["loglevel"].as<std::string>()));
            
            if (log) 
                loglevel = *log;
            else {
                std::cerr << "error: " << vm["loglevel"].as<std::string>() << " is not a valid loglevel" << std::endl;
                std::cerr << desc << std::endl;
                return EXIT_FAILURE;
            }                            

            visberry::set_log_level(loglevel);

        } else {
            visberry::set_log_level(LOG_LEVEL(error));
        }

        if (vm.count("name"))
           name = vm["name"].as<std::string>(); 

    } catch (const boost_args::error& err) {
        std::cerr << "error: " << err.what() << std::endl << std::endl;
        std::cerr << desc << std::endl;
        return EXIT_FAILURE;
    }
    
    webcamlog_daemon daemon(configuration_file);

    if (!daemon.load_configuration()) {
        LOG(error) << "Initial configuration process failed. Don't run webcamlog daemon";
        return EXIT_FAILURE;
    }

    if (vm.count("loglevel"))
        visberry::set_log_level(loglevel);

    daemon.connect();

    return EXIT_SUCCESS;    
}

