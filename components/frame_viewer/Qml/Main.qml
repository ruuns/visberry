import QtQuick 2.3
import FrameView 1.0

Item {
    width: 640
    height: 480

    Text {
        text: "Hello World"
        color: "black"

        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: 20
    }

    FrameView {
        SequentialAnimation {
            running: true
        }
    }


}
