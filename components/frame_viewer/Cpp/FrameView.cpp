#include "FrameView.hpp"

#include <QtQuick/QQuickWindow>
#include <QtGui/QOpenGLContext>
#include <QtCore/QDebug>


FrameRenderer::FrameRenderer() 
  : m_viewport(0, 0)
{
}


FrameRenderer::~FrameRenderer() 
{
}


void FrameRenderer::setViewport(const QSize& size)
{
    m_viewport = size;

    m_data.resize(size.width() * size.height() * 3);
    m_data.fill(0x0F);
}



void FrameRenderer::paint()
{
    static bool initialized(false);
    static uint32_t gl_rgb_texture(0);

    if (!initialized) {
        initializeOpenGLFunctions();
        initialized = true;
    
        glGenTextures(1, &gl_rgb_texture);
    }

    qDebug() << "Viewport size: " << m_viewport;
    // gl init
    glViewport(0, 0, m_viewport.width(), m_viewport.height());

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, m_viewport.width(), m_viewport.height(), 0, 0.0f, 1.0f);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();

    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT);
    glEnable(GL_TEXTURE_2D);

    //glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glBindTexture(GL_TEXTURE_2D, gl_rgb_texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glBindTexture(GL_TEXTURE_2D, 0);

    glBindTexture(GL_TEXTURE_2D, gl_rgb_texture);
    glTexImage2D(GL_TEXTURE_2D, 0, 3, m_viewport.width(), m_viewport.height(), 0, GL_RGB, GL_UNSIGNED_BYTE, m_data.data());

    glLoadIdentity();
    glColor4f(1, 1, 1, 1);

    glBegin(GL_QUADS);
    // glColor4f(1, 1, 1, 1);
    glTexCoord2f(0, 0); glVertex3f(0, 0, 0);
    glTexCoord2f(1, 0); glVertex3f(m_viewport.width(), 0, 0);
    glTexCoord2f(1, 1); glVertex3f(m_viewport.width(), m_viewport.height(), 0);
    glTexCoord2f(0, 1); glVertex3f(0, m_viewport.height(), 0);
    glEnd();

    glDisable(GL_TEXTURE_2D);
    glPopMatrix();
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);

}




FrameView::FrameView()
  : m_renderer(nullptr)
{
    connect(this, SIGNAL(windowChanged(QQuickWindow*)), this, SLOT(handleWindowChanged(QQuickWindow*)), Qt::DirectConnection);
}

void FrameView::handleWindowChanged(QQuickWindow* win)
{
    if (win) 
    {
        win->setClearBeforeRendering(false);

        connect(win, SIGNAL(beforeSynchronizing()), this, SLOT(setup()), Qt::DirectConnection);
        connect(win, SIGNAL(sceneGraphInvalidated()), this, SLOT(cleanup()), Qt::DirectConnection);
    }
}

void FrameView::setup()
{
    if (!m_renderer) 
    {
        m_renderer = new FrameRenderer();
        connect(window(), SIGNAL(beforeRendering()), m_renderer, SLOT(paint()), Qt::DirectConnection);
    }

    m_renderer->setViewport(window()->size());
}

void FrameView::cleanup()
{
    if (m_renderer) 
    {
        delete m_renderer;
        m_renderer = nullptr;
    }
}
