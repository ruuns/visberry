#include <QGuiApplication>
#include <QtQuick/QQuickView>
#include "FrameView.hpp"

int main(int argc, char** argv)
{
    QGuiApplication app(argc, argv);

    qmlRegisterType<FrameView>("FrameView", 1, 0, "FrameView");

    QQuickView view;
    view.setResizeMode(QQuickView::SizeRootObjectToView);
    view.setSource(QUrl("qrc:/Qml/Main.qml"));
    view.show();

    return app.exec();
}
