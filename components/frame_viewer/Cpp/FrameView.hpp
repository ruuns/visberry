#pragma once

#include <QtQuick/QQuickItem>
#include <QtGui/QOpenGLFunctions>


class FrameRenderer : public QObject, protected QOpenGLFunctions 
{
    Q_OBJECT

public:
    FrameRenderer();
    ~FrameRenderer();

    void setViewport(const QSize& size);

public slots:
    void paint();

private:
    QSize m_viewport;
    QByteArray m_data; 
};



class FrameView : public QQuickItem 
{
    Q_OBJECT
public:
    FrameView();

public slots:
    void setup();
    void cleanup();

private slots:
    void handleWindowChanged(QQuickWindow* win);


private:
    FrameRenderer* m_renderer;
};


