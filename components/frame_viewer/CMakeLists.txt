cmake_minimum_required(VERSION 3.0)

project(frame_viewer)

find_package(Qt5 COMPONENTS Quick Core)
find_package(OpenGL REQUIRED)

set(CMAKE_AUTOMOC on)
set(CMAKE_INCLUDE_CURRENT_DIR on)

qt5_add_resources(RESOURCES ${PROJECT_NAME}.qrc)

set(SOURCES 
    Cpp/Main.cpp
    Cpp/FrameView.cpp)

add_executable(${PROJECT_NAME} ${RESOURCES} ${SOURCES})

target_link_libraries(${PROJECT_NAME} Qt5::Quick Qt5::Core ${OPENGL_gl_LIBRARY})
