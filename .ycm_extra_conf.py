
def FlagsForFile(filename, **kwargs):
    include_paths = [ 
            '-I.', 
            '-I/usr/include', 
            '-I/usr/include/qt/i',
            '-I./libs/webcam-v4l2/source',
            '-I./libs'
    ]

    flags = ['-Wall', '-std=c++11', '-x', 'c++'] + include_paths
    return { 'flags': flags, 'do_cache':  True }


